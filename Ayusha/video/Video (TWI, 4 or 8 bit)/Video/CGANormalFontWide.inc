;****************************************************** 
; COMPLETE CGA NORMAL FONT - WIDE VERSION
;
; Extracted by Grant Searle 2013 added CP866 by RW6HRM 2018
;******************************************************
.CSEG
.ORG 0x1800

; 40 Char / line font (double width) left half of each char

; Character line 0:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x3F, 0x3F, 0x3C, 0x03, 0x0F, 0x03, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0x0F, 0x0F, 0x0F, 0x03
.db  0xC0, 0x00, 0x03, 0x0C, 0x3F, 0x0F, 0x00, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x03, 0x0C, 0x0C, 0x03, 0x00, 0x0F, 0x03, 0x03, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x0F, 0x03, 0x0F, 0x0F, 0x00, 0x3F, 0x03, 0x3F, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F
.db  0x0F, 0x03, 0x3F, 0x03, 0x3F, 0x3F, 0x3F, 0x03, 0x30, 0x0F, 0x00, 0x3C, 0x3F, 0x3C, 0x3C, 0x03
.db  0x3F, 0x0F, 0x3F, 0x0F, 0x3F, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3F, 0x3F, 0xC0, 0x3F, 0x03, 0x00
.db  0x03, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x03, 0x00, 0x3C, 0x0F, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F, 0x0F, 0x00
.db  0x00, 0x3F, 0x3F, 0x3F, 0x0F, 0x3F, 0x61, 0x1F, 0x30, 0x33, 0x30, 0x00, 0x30, 0x30, 0x1F, 0x3F
.db  0x3F, 0x1F, 0x3F, 0x30, 0x01, 0x30, 0x30, 0x30, 0x31, 0x31, 0xF0, 0x30, 0x30, 0x1F, 0x31, 0x1F
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x0C, 0x33, 0xF3, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00
.db  0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03
.db  0x03, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x06, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x30, 0x00, 0x00, 0x00

; Character line 1:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xC0, 0xFF, 0xFF, 0x0F, 0x3F, 0x03, 0x00, 0xFF, 0x0F, 0xF0, 0x00, 0x30, 0x0C, 0x0C, 0xF3
.db  0xFC, 0x00, 0x0F, 0x0C, 0xC3, 0x3C, 0x00, 0x0F, 0x0F, 0x03, 0x03, 0x0F, 0x00, 0x0C, 0x03, 0xFF
.db  0x00, 0x0F, 0x0C, 0x0C, 0x0F, 0x3C, 0x30, 0x03, 0x0C, 0x03, 0x30, 0x03, 0x00, 0x00, 0x00, 0x00
.db  0x30, 0x0F, 0x30, 0x30, 0x03, 0x30, 0x0C, 0x30, 0x30, 0x30, 0x03, 0x03, 0x03, 0x00, 0x00, 0x30
.db  0x30, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x30, 0x03, 0x00, 0x0C, 0x0C, 0x33, 0x33, 0x0C
.db  0x0C, 0x30, 0x0C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x0C, 0x0C, 0x30, 0x30, 0x30, 0x00, 0x0C, 0x00
.db  0x03, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x03, 0x00, 0x0C, 0x00, 0x00, 0x0C, 0x03, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x30, 0x00
.db  0x00, 0x30, 0x30, 0x30, 0x0C, 0x30, 0x19, 0x30, 0x30, 0x30, 0x30, 0x00, 0x3C, 0x30, 0x30, 0x30
.db  0x30, 0x30, 0x01, 0x30, 0x1F, 0x30, 0x30, 0x30, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x33, 0x30
.db  0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xC0, 0xCC, 0x3F, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00
.db  0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03
.db  0x03, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x01, 0x03, 0x03, 0x03, 0x30, 0x3C, 0x00, 0x00

; Character line 2:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xCC, 0xF3, 0xFF, 0x3F, 0x0F, 0x0F, 0x03, 0xFC, 0x3C, 0xC3, 0x00, 0x30, 0x0F, 0x0F, 0x0F
.db  0xFF, 0x0F, 0x3F, 0x0C, 0xC3, 0x0F, 0x00, 0x3F, 0x3F, 0x03, 0x00, 0x3C, 0x30, 0x3C, 0x0F, 0xFF
.db  0x00, 0x0F, 0x0C, 0x3F, 0x30, 0x3C, 0x0F, 0x0C, 0x30, 0x00, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00
.db  0x30, 0x33, 0x00, 0x00, 0x0C, 0x3F, 0x30, 0x00, 0x30, 0x30, 0x03, 0x03, 0x0C, 0x3F, 0x00, 0x00
.db  0x33, 0x30, 0x0C, 0x30, 0x0C, 0x0C, 0x0C, 0x30, 0x30, 0x03, 0x00, 0x0C, 0x0C, 0x30, 0x30, 0x30
.db  0x0C, 0x30, 0x0C, 0x30, 0x00, 0x30, 0x30, 0x30, 0x03, 0x03, 0x00, 0x30, 0x0C, 0x00, 0x30, 0x00
.db  0x00, 0x0F, 0x0C, 0x0F, 0x00, 0x0F, 0x03, 0x0F, 0x0C, 0x0F, 0x00, 0x0C, 0x03, 0x3F, 0x33, 0x0F
.db  0x3C, 0x0F, 0x3C, 0x0F, 0x3F, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3F, 0x03, 0x03, 0x00, 0x00, 0x03
.db  0x03, 0x30, 0x30, 0x30, 0x0C, 0x30, 0x19, 0x00, 0x30, 0x30, 0x30, 0x03, 0x33, 0x30, 0x30, 0x30
.db  0x30, 0x30, 0x01, 0x30, 0x31, 0x0C, 0x30, 0x30, 0x31, 0x31, 0x30, 0x30, 0x30, 0x00, 0x33, 0x30
.db  0x0F, 0x30, 0x3F, 0x3F, 0x0F, 0x1F, 0x31, 0x0F, 0x30, 0x30, 0x30, 0x00, 0x30, 0x30, 0x1F, 0x3F
.db  0x0C, 0x33, 0xF3, 0x03, 0x03, 0xFF, 0x03, 0x00, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0x03, 0xFF, 0x00
.db  0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0xFF
.db  0x03, 0xFF, 0x00, 0x03, 0x03, 0x03, 0x00, 0x03, 0xFF, 0x03, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF
.db  0x3F, 0x1F, 0x3F, 0x30, 0x1F, 0x38, 0x30, 0x30, 0x31, 0x31, 0xF0, 0x30, 0x30, 0x1F, 0x31, 0x1F
.db  0x3F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x01, 0x0F, 0x00, 0x1F, 0x3C, 0x03, 0x0F, 0x00

; Character line 3:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xC0, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0x0F, 0xF0, 0x30, 0xCF, 0x3F, 0x30, 0x0C, 0x0C, 0xFC
.db  0xFF, 0xFF, 0x03, 0x0C, 0x3F, 0x30, 0x00, 0x03, 0x33, 0x33, 0xFF, 0xFF, 0x30, 0xFF, 0x3F, 0x3F
.db  0x00, 0x03, 0x00, 0x0C, 0x0F, 0x00, 0x33, 0x00, 0x30, 0x00, 0xFF, 0x3F, 0x00, 0x3F, 0x00, 0x00
.db  0x30, 0x03, 0x00, 0x03, 0x30, 0x00, 0x3F, 0x00, 0x0F, 0x0F, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00
.db  0x33, 0x30, 0x0F, 0x30, 0x0C, 0x0F, 0x0F, 0x30, 0x3F, 0x03, 0x00, 0x0F, 0x0C, 0x30, 0x30, 0x30
.db  0x0F, 0x30, 0x0F, 0x0F, 0x00, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x30, 0x03, 0x00, 0xC0, 0x00
.db  0x00, 0x00, 0x0C, 0x30, 0x0F, 0x30, 0x0F, 0x30, 0x0F, 0x03, 0x00, 0x0C, 0x03, 0x30, 0x3C, 0x30
.db  0x0F, 0x30, 0x0F, 0x30, 0x03, 0x30, 0x30, 0x30, 0x0C, 0x30, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x0C
.db  0x0C, 0x3F, 0x3F, 0x30, 0x0C, 0x3F, 0x07, 0x0F, 0x30, 0x30, 0x3F, 0x0C, 0x31, 0x3F, 0x30, 0x30
.db  0x3F, 0x30, 0x01, 0x30, 0x31, 0x03, 0x30, 0x1F, 0x31, 0x31, 0x3F, 0x3F, 0x3F, 0x03, 0x3F, 0x1F
.db  0x00, 0x3F, 0x30, 0x30, 0x0C, 0x30, 0x1D, 0x30, 0x30, 0x30, 0x30, 0x03, 0x3C, 0x30, 0x30, 0x30
.db  0xC0, 0xCC, 0xFC, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00, 0x03, 0x03, 0x00
.db  0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00
.db  0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF
.db  0x30, 0x30, 0x01, 0x30, 0x31, 0x0E, 0x30, 0x30, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x33, 0x30
.db  0x30, 0x30, 0x00, 0xC0, 0x00, 0x00, 0x3F, 0x3F, 0x01, 0x1D, 0x1F, 0x03, 0x33, 0x0C, 0x0F, 0x00

; Character line 4:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xCF, 0xF0, 0x3F, 0x3F, 0xFF, 0xFF, 0x0F, 0xF0, 0x30, 0xCF, 0xC0, 0x0F, 0x0C, 0x0C, 0xFC
.db  0xFF, 0x0F, 0x03, 0x0C, 0x03, 0x30, 0x3F, 0x3F, 0x03, 0x3F, 0x00, 0x3C, 0x30, 0x3C, 0xFF, 0x0F
.db  0x00, 0x03, 0x00, 0x3F, 0x00, 0x03, 0xC0, 0x00, 0x30, 0x00, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x03
.db  0x33, 0x03, 0x0F, 0x00, 0xFF, 0x00, 0x30, 0x03, 0x30, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00
.db  0x33, 0x3F, 0x0C, 0x30, 0x0C, 0x0C, 0x0C, 0x30, 0x30, 0x03, 0x30, 0x0C, 0x0C, 0x30, 0x30, 0x30
.db  0x0C, 0x30, 0x0C, 0x00, 0x00, 0x30, 0x0C, 0x30, 0x03, 0x00, 0x03, 0x30, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x0F, 0x0F, 0x30, 0x30, 0x3F, 0x03, 0x30, 0x0C, 0x03, 0x00, 0x0F, 0x03, 0x30, 0x30, 0x30
.db  0x0F, 0x30, 0x0C, 0x0F, 0x03, 0x30, 0x0C, 0x30, 0x03, 0x30, 0x03, 0x03, 0x03, 0x00, 0x00, 0x30
.db  0x3F, 0x30, 0x30, 0x30, 0x0C, 0x30, 0x19, 0x00, 0x33, 0x33, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30
.db  0x30, 0x30, 0x01, 0x1F, 0x31, 0x0C, 0x30, 0x00, 0x31, 0x31, 0x30, 0x30, 0x30, 0x00, 0x33, 0x06
.db  0x1F, 0x30, 0x3F, 0x30, 0x0C, 0x3F, 0x07, 0x00, 0x30, 0x30, 0x3F, 0x0C, 0x33, 0x3F, 0x30, 0x30
.db  0x0C, 0x33, 0xF3, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
.db  0x03, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0xFF
.db  0xFF, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xFF, 0x03, 0xFF, 0xFF, 0xFF, 0x00, 0x00
.db  0x3F, 0x30, 0x01, 0x1F, 0x31, 0x07, 0x30, 0x1F, 0x31, 0x31, 0x3F, 0x3F, 0x3F, 0x01, 0x3F, 0x1F
.db  0x3F, 0x3F, 0x00, 0x30, 0x00, 0x00, 0x3F, 0x3F, 0x1D, 0x01, 0x00, 0x03, 0x30, 0x0C, 0x0F, 0x00

; Character line 5:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xC3, 0xFC, 0x0F, 0x0F, 0xF3, 0x3F, 0x03, 0xFC, 0x3C, 0xC3, 0xC0, 0x03, 0x3C, 0x3C, 0x0F
.db  0xFC, 0x00, 0x3F, 0x00, 0x03, 0x0F, 0x3F, 0x0F, 0x03, 0x0F, 0x03, 0x0F, 0x3F, 0x0C, 0xFF, 0x03
.db  0x00, 0x00, 0x00, 0x0C, 0x3F, 0x0C, 0xC0, 0x00, 0x0C, 0x03, 0x30, 0x03, 0x03, 0x00, 0x03, 0x0C
.db  0x3C, 0x03, 0x30, 0x30, 0x00, 0x30, 0x30, 0x03, 0x30, 0x00, 0x03, 0x03, 0x03, 0x3F, 0x00, 0x00
.db  0x30, 0x30, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x30, 0x03, 0x30, 0x0C, 0x0C, 0x30, 0x30, 0x0C
.db  0x0C, 0x0F, 0x0C, 0x30, 0x00, 0x30, 0x03, 0x30, 0x0C, 0x00, 0x0C, 0x30, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x30, 0x0F, 0x30, 0x30, 0x30, 0x03, 0x0F, 0x0C, 0x03, 0x30, 0x0C, 0x03, 0x30, 0x30, 0x30
.db  0x0C, 0x0F, 0x0C, 0x00, 0x03, 0x30, 0x03, 0x30, 0x0C, 0x0F, 0x0C, 0x03, 0x03, 0x00, 0x00, 0x30
.db  0x30, 0x30, 0x30, 0x30, 0x3F, 0x30, 0x19, 0x30, 0x3C, 0x3C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30
.db  0x30, 0x30, 0x01, 0x00, 0x1F, 0x30, 0x30, 0x00, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x33, 0x1C
.db  0x30, 0x30, 0x30, 0x30, 0x3F, 0x30, 0x1D, 0x30, 0x33, 0x33, 0x30, 0x30, 0x31, 0x30, 0x30, 0x30
.db  0xC0, 0xCC, 0x3F, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03
.db  0x00, 0x00, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x00, 0x03, 0x00
.db  0x00, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0x00, 0x00
.db  0x30, 0x30, 0x01, 0x00, 0x1F, 0x0E, 0x30, 0x00, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x33, 0x0C
.db  0x30, 0x30, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x18, 0x0F, 0x01, 0x03, 0x00, 0x30, 0x03, 0x0F, 0x00

; Character line 6:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xC0, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x00, 0xFF, 0x0F, 0xF0, 0xC0, 0x3F, 0xFC, 0xFC, 0xF3
.db  0xC0, 0x00, 0x0F, 0x0C, 0x03, 0xF0, 0x3F, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x03, 0x00, 0x0C, 0x03, 0x30, 0x3F, 0x00, 0x03, 0x0C, 0x00, 0x00, 0x03, 0x00, 0x03, 0x30
.db  0x0F, 0x3F, 0x3F, 0x0F, 0x03, 0x0F, 0x0F, 0x03, 0x0F, 0x0F, 0x03, 0x03, 0x00, 0x00, 0x03, 0x00
.db  0x0F, 0x30, 0x3F, 0x03, 0x3F, 0x3F, 0x3F, 0x03, 0x30, 0x0F, 0x0F, 0x3C, 0x3F, 0x30, 0x30, 0x03
.db  0x3F, 0x00, 0x3F, 0x0F, 0x03, 0x0F, 0x00, 0x0F, 0x30, 0x03, 0x3F, 0x3F, 0x00, 0x3F, 0x00, 0x00
.db  0x00, 0x0F, 0x0C, 0x0F, 0x0F, 0x0F, 0x0F, 0x00, 0x3C, 0x0F, 0x30, 0x0C, 0x0F, 0x30, 0x30, 0x0F
.db  0x0C, 0x00, 0x3F, 0x3F, 0x00, 0x0F, 0x00, 0x0F, 0x30, 0x00, 0x3F, 0x00, 0x03, 0x0F, 0x00, 0x3F
.db  0x30, 0x3F, 0x3F, 0x30, 0x30, 0x3F, 0x61, 0x1F, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x1F, 0x30
.db  0x30, 0x1F, 0x01, 0x0F, 0x01, 0x30, 0x3F, 0x00, 0x3F, 0x31, 0x3F, 0x3F, 0x3F, 0x1F, 0x31, 0x30
.db  0x1F, 0x1F, 0x3F, 0x30, 0x30, 0x1F, 0x31, 0x0F, 0x3C, 0x3C, 0x30, 0x30, 0x30, 0x30, 0x1F, 0x30
.db  0x0C, 0x33, 0xF3, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03
.db  0x00, 0x00, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x00, 0x03, 0x00
.db  0x00, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0x00, 0x00
.db  0x30, 0x1F, 0x01, 0x0F, 0x01, 0x38, 0x3F, 0x00, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x1F, 0x31, 0x30
.db  0x3F, 0x1F, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0C, 0x03, 0x01, 0x03, 0x3F, 0x30, 0x3C, 0x00, 0x00

; Character line 7:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x3F, 0x3F, 0x00, 0x00, 0x0F, 0x0F, 0x00, 0xFF, 0x00, 0xFF, 0x3F, 0x03, 0xF0, 0xF0, 0x03
.db  0x00, 0x00, 0x03, 0x00, 0x00, 0x3F, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xC0, 0xCC, 0xFC, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03
.db  0x00, 0x00, 0x03, 0x03, 0x00, 0x03, 0x03, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03, 0x00, 0x03, 0x00
.db  0x00, 0x03, 0x03, 0x00, 0x00, 0x03, 0x03, 0x03, 0x03, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

; 40 Char / line font (double width) right half of each char

; Character line 0:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xFC, 0xFC, 0xF0, 0x00, 0xC0, 0x00, 0x00, 0xFF, 0x00, 0xFF, 0xFF, 0xF0, 0xFF, 0xFF, 0xC0
.db  0x00, 0x0C, 0xC0, 0x30, 0xFF, 0xFC, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x30, 0x30, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xF0, 0x00, 0xF0, 0xF0, 0xC0, 0xFC, 0xF0, 0xFC, 0xF0, 0xF0, 0x00, 0x00, 0xC0, 0x00, 0x00, 0xF0
.db  0xF0, 0xC0, 0xF0, 0xF0, 0xC0, 0xFC, 0xFC, 0xF0, 0x0C, 0xC0, 0xFC, 0x0C, 0x00, 0x0F, 0x0C, 0xC0
.db  0xF0, 0xF0, 0xF0, 0xF0, 0xFF, 0x0C, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xC0, 0x00, 0xC0, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x0C, 0x00
.db  0x3C, 0xF0, 0xE0, 0xF0, 0xF0, 0xF8, 0x86, 0xF8, 0x0C, 0xCC, 0x0C, 0x3C, 0x0C, 0x0C, 0xF8, 0xFC
.db  0xF8, 0xF8, 0xFC, 0x0C, 0x80, 0x0C, 0x30, 0x0C, 0x8C, 0x8C, 0x00, 0x0C, 0x00, 0xF8, 0xF8, 0xFC
.db  0x00, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x0C, 0x33, 0xCF, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x30, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30, 0x00
.db  0x30, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x60, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0x3F, 0x00, 0x00, 0x00

; Character line 1:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x03, 0xFF, 0xFC, 0xC0, 0xF0, 0x00, 0x00, 0xFF, 0xF0, 0x0F, 0x0F, 0x0C, 0x03, 0x03, 0xCF
.db  0x00, 0xFC, 0xF0, 0x30, 0x0C, 0x0F, 0x00, 0xF0, 0xC0, 0x00, 0xC0, 0x00, 0x00, 0x30, 0x00, 0xFC
.db  0x00, 0xC0, 0x30, 0x30, 0xFC, 0x0C, 0xC0, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x0C
.db  0x0C, 0x00, 0x0C, 0x0C, 0xC0, 0x00, 0x00, 0x0C, 0x0C, 0x0C, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x0C
.db  0x0C, 0x30, 0x0C, 0x0C, 0x30, 0x0C, 0x0C, 0x0C, 0x0C, 0x00, 0x30, 0x30, 0x00, 0x33, 0x0C, 0x30
.db  0x0C, 0x0C, 0x0C, 0x0C, 0xC3, 0x0C, 0x03, 0x03, 0x0C, 0x0C, 0x0C, 0x00, 0x00, 0xC0, 0xC0, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xC0
.db  0xCC, 0x00, 0x30, 0x00, 0x30, 0x00, 0x98, 0x0C, 0x0C, 0x0C, 0x30, 0xCC, 0x3C, 0x0C, 0x0C, 0x0C
.db  0x0C, 0x0C, 0x80, 0x0C, 0xF8, 0x0C, 0x30, 0x0C, 0x8C, 0x8C, 0x00, 0x0C, 0x00, 0x0C, 0x0C, 0x0C
.db  0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xC0, 0xCC, 0x3F, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x30, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30, 0x00
.db  0x30, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x03, 0xC0, 0x30, 0x00, 0x80, 0xC0, 0xC0, 0xC0, 0x39, 0x3C, 0x00, 0x00

; Character line 2:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x33, 0xCF, 0xFC, 0xF0, 0xC0, 0xC0, 0xC0, 0x3F, 0x3C, 0xC3, 0x33, 0x0C, 0xFF, 0xFF, 0xF0
.db  0xC0, 0xFC, 0xFC, 0x30, 0x0C, 0xC0, 0x00, 0xFC, 0xF0, 0x00, 0xF0, 0x00, 0x00, 0x3C, 0xC0, 0xFC
.db  0x00, 0xC0, 0x30, 0xFC, 0x00, 0x30, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x30
.db  0x3C, 0x00, 0x0C, 0x0C, 0xC0, 0xF0, 0x00, 0x30, 0x0C, 0x0C, 0x00, 0x00, 0x00, 0xFC, 0x30, 0x0C
.db  0xFC, 0x0C, 0x0C, 0x00, 0x0C, 0xC0, 0xC0, 0x00, 0x0C, 0x00, 0x30, 0xC0, 0x00, 0xC3, 0xCC, 0x0C
.db  0x0C, 0x0C, 0x0C, 0x00, 0xC0, 0x0C, 0x03, 0x03, 0x30, 0x30, 0x30, 0x00, 0x00, 0xC0, 0x30, 0x00
.db  0xC0, 0xF0, 0x00, 0xF0, 0x0C, 0xF0, 0x00, 0xF3, 0xF0, 0x00, 0x3C, 0x30, 0x00, 0x3C, 0xF0, 0xF0
.db  0xF0, 0x3C, 0xF0, 0xFC, 0xF0, 0x0C, 0x03, 0x03, 0x30, 0x0C, 0xF0, 0x00, 0x00, 0xC0, 0x00, 0x30
.db  0x0C, 0x00, 0x30, 0x00, 0x30, 0x00, 0x98, 0x0C, 0x3C, 0x3C, 0xC0, 0x0C, 0xCC, 0x0C, 0x0C, 0x0C
.db  0x0C, 0x00, 0x80, 0x0C, 0x8C, 0x30, 0x30, 0x0C, 0x8C, 0x8C, 0x00, 0x0C, 0x00, 0x0C, 0x0C, 0x0C
.db  0xE0, 0x00, 0xF8, 0xF0, 0xF0, 0xF8, 0x8C, 0xF0, 0x0C, 0x0C, 0x1C, 0xFC, 0x0C, 0x0C, 0xF8, 0xFC
.db  0x0C, 0x33, 0xCF, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x30, 0x30, 0xF0, 0x30, 0x30, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x30, 0x3F, 0xFF, 0x3F, 0xFF, 0x3F, 0xFF, 0x3F, 0xFF
.db  0x30, 0xFF, 0x00, 0x30, 0xFF, 0xFF, 0x00, 0x30, 0xFF, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF
.db  0xF8, 0xF8, 0xF8, 0x0C, 0xF8, 0x1C, 0x18, 0x0C, 0x8C, 0x8C, 0x00, 0x0C, 0x00, 0xF8, 0xF8, 0xFC
.db  0xFC, 0xF8, 0x00, 0x00, 0x0C, 0x30, 0x18, 0x00, 0x80, 0xF0, 0x00, 0xF8, 0x39, 0xC0, 0xF0, 0x00

; Character line 3:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x03, 0xFF, 0xFC, 0xFC, 0xFC, 0xF0, 0xF0, 0x0F, 0x0C, 0xF3, 0xF3, 0x0C, 0x00, 0x03, 0x3F
.db  0xFC, 0xFC, 0xC0, 0x30, 0x0C, 0x30, 0x00, 0xC0, 0x30, 0x30, 0xFC, 0xFC, 0x00, 0xFF, 0xF0, 0xF0
.db  0x00, 0x00, 0x00, 0x30, 0xF0, 0xC0, 0x3C, 0x00, 0x00, 0xC0, 0xFC, 0xF0, 0x00, 0xFC, 0x00, 0xC0
.db  0xCC, 0x00, 0xF0, 0xF0, 0xC0, 0x0C, 0xF0, 0xC0, 0xF0, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x30
.db  0x0C, 0x0C, 0xF0, 0x00, 0x0C, 0xC0, 0xC0, 0x00, 0xFC, 0x00, 0x30, 0x00, 0x00, 0x03, 0x3C, 0x0C
.db  0xF0, 0x0C, 0xF0, 0xF0, 0xC0, 0x0C, 0x03, 0xC3, 0xC0, 0xC0, 0xC0, 0x00, 0x00, 0xC0, 0x0C, 0x00
.db  0x00, 0x0C, 0xFC, 0x0C, 0xCC, 0x0C, 0xC0, 0x0C, 0x0C, 0x00, 0x0C, 0xC0, 0x00, 0xC3, 0x0C, 0x0C
.db  0x0C, 0xF0, 0x0C, 0x00, 0x00, 0x0C, 0x03, 0xC3, 0xC0, 0x0C, 0xC0, 0x00, 0x00, 0x3C, 0x00, 0x0C
.db  0x0C, 0xF8, 0xF8, 0x00, 0x30, 0xE0, 0xE0, 0xF8, 0xCC, 0xCC, 0x00, 0x0C, 0x8C, 0xFC, 0x0C, 0x0C
.db  0xF8, 0x00, 0x80, 0x0C, 0x8C, 0xC0, 0x30, 0xFC, 0x8C, 0x8C, 0xF8, 0x8C, 0xF8, 0xFC, 0x0C, 0xFC
.db  0x30, 0xF8, 0x0C, 0x00, 0x30, 0x0C, 0xB8, 0x0C, 0x3C, 0x3C, 0x70, 0x0C, 0x3C, 0x0C, 0x0C, 0x0C
.db  0xC0, 0xCC, 0xFC, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x30, 0x30, 0x30, 0x30, 0x30, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x30, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF
.db  0x0C, 0x0C, 0x80, 0x0C, 0x8C, 0x70, 0x18, 0x0C, 0x8C, 0x8C, 0x00, 0x0C, 0x00, 0x0C, 0x0C, 0x0C
.db  0x00, 0x0C, 0x03, 0x00, 0x30, 0x0C, 0xFC, 0xFC, 0x80, 0xB8, 0xF8, 0xC0, 0x3F, 0x30, 0xF0, 0x00

; Character line 4:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xF3, 0x0F, 0xF0, 0xF0, 0xFC, 0xFC, 0xF0, 0x0F, 0x0C, 0xF3, 0x30, 0xF0, 0x00, 0x0F, 0x3F
.db  0xC0, 0xFC, 0xC0, 0x30, 0x0C, 0x30, 0xFC, 0xFC, 0x00, 0xF0, 0xF0, 0x00, 0x00, 0x3C, 0xFC, 0xC0
.db  0x00, 0x00, 0x00, 0xFC, 0x0C, 0x00, 0xC0, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x0C, 0x00, 0x00, 0x0C, 0xFC, 0x0C, 0x0C, 0x00, 0x0C, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x30, 0xC0
.db  0xFC, 0xFC, 0x0C, 0x00, 0x0C, 0xC0, 0xC0, 0xFC, 0x0C, 0x00, 0x30, 0xC0, 0x00, 0x03, 0x0C, 0x0C
.db  0x00, 0xCC, 0xC0, 0x0C, 0xC0, 0x0C, 0x0C, 0xC3, 0x30, 0xC0, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00
.db  0x00, 0xFC, 0x03, 0x00, 0x3C, 0xFC, 0x00, 0x0C, 0x0C, 0x00, 0x0C, 0x00, 0x00, 0xC3, 0x0C, 0x0C
.db  0x0C, 0xF0, 0x0C, 0xF0, 0x00, 0x0C, 0x0C, 0xC3, 0x00, 0x0C, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x03
.db  0xFC, 0x0C, 0x0C, 0x00, 0x30, 0x00, 0x98, 0x0C, 0x0C, 0x0C, 0xC0, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C
.db  0x00, 0x00, 0x80, 0xFC, 0x8C, 0x30, 0x30, 0x0C, 0x8C, 0x8C, 0x0C, 0xCC, 0x0C, 0x0C, 0x0C, 0x0C
.db  0xF0, 0x0C, 0xF8, 0x00, 0x30, 0xFC, 0xE0, 0xF0, 0xCC, 0xCC, 0xC0, 0x0C, 0xCC, 0xFC, 0x0C, 0x0C
.db  0x0C, 0x33, 0xCF, 0x00, 0x00, 0x00, 0x30, 0xF0, 0x00, 0x30, 0x30, 0x30, 0xF0, 0xF0, 0x00, 0x00
.db  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0x3F, 0xFF, 0x3F, 0x3F, 0xFF, 0x3F, 0xFF
.db  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0x00
.db  0xF8, 0x00, 0x80, 0xFC, 0x8C, 0xE0, 0x18, 0xFC, 0x8C, 0x8C, 0xF8, 0xCC, 0xF8, 0xFC, 0x0C, 0xFC
.db  0xF0, 0xFC, 0x0C, 0x00, 0xC0, 0x03, 0xFC, 0xFC, 0xB8, 0x80, 0x00, 0xC0, 0xF0, 0x30, 0xF0, 0x00

; Character line 5:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xC3, 0x3F, 0xC0, 0xC0, 0x3C, 0xF0, 0xC0, 0x3F, 0x3C, 0xC3, 0x30, 0xC0, 0x00, 0x3F, 0xF0
.db  0x00, 0xFC, 0xFC, 0x00, 0x0C, 0xC0, 0xFC, 0xF0, 0x00, 0xC0, 0xC0, 0x00, 0xFC, 0x30, 0xFC, 0x00
.db  0x00, 0x00, 0x00, 0x30, 0xF0, 0x3C, 0xC0, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x0C, 0x00, 0x0C, 0x0C, 0xC0, 0x0C, 0x0C, 0x00, 0x0C, 0x30, 0x00, 0x00, 0x00, 0xFC, 0xC0, 0x00
.db  0x00, 0x0C, 0x0C, 0x0C, 0x30, 0x0C, 0x00, 0x0C, 0x0C, 0x00, 0x30, 0x30, 0x0C, 0x03, 0x0C, 0x30
.db  0x00, 0xF0, 0x30, 0x0C, 0xC0, 0x0C, 0x30, 0xC3, 0x0C, 0xC0, 0x03, 0x00, 0x30, 0xC0, 0x00, 0x00
.db  0x00, 0x0C, 0x03, 0x0C, 0x3C, 0x00, 0x00, 0xFC, 0x0C, 0x00, 0x0C, 0xC0, 0x00, 0xC3, 0x0C, 0x0C
.db  0xF0, 0x30, 0x00, 0x0C, 0x0C, 0x3C, 0x30, 0xC3, 0xC0, 0xFC, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x03
.db  0x0C, 0x0C, 0x0C, 0x00, 0xFC, 0x00, 0x98, 0x0C, 0x0C, 0x0C, 0x30, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C
.db  0x00, 0x0C, 0x80, 0x0C, 0xF8, 0x0C, 0x30, 0x0C, 0x8C, 0x8C, 0x0C, 0xCC, 0x0C, 0x0C, 0x0C, 0x0C
.db  0x30, 0x0C, 0x0C, 0x00, 0xFC, 0x00, 0xB8, 0x0C, 0x0C, 0x0C, 0x70, 0x0C, 0x8C, 0x0C, 0x0C, 0x0C
.db  0xC0, 0xCC, 0x3F, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30, 0x30, 0x00, 0x30, 0x00
.db  0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0x00
.db  0x00, 0x0C, 0x80, 0x0C, 0xF8, 0x70, 0x18, 0x0C, 0x8C, 0x8C, 0x0C, 0x6C, 0x0C, 0x0C, 0x0C, 0x0C
.db  0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x18, 0x00, 0xF0, 0x80, 0xC0, 0x00, 0x3F, 0xC0, 0xF0, 0x00

; Character line 6:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0x03, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xF0, 0x0F, 0x30, 0xFC, 0x00, 0x3C, 0xCF
.db  0x00, 0x0C, 0xF0, 0x30, 0x0C, 0xF0, 0xFC, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x30, 0xC0, 0x3C, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xF0, 0xF0, 0xFC, 0xF0, 0xF0, 0xF0, 0xF0, 0x00, 0xF0, 0xC0, 0x00, 0x00, 0xC0, 0x00, 0x00, 0xC0
.db  0xF0, 0x0C, 0xF0, 0xF0, 0xC0, 0xFC, 0x00, 0xFC, 0x0C, 0xC0, 0xC0, 0x0F, 0xFC, 0x03, 0x0C, 0xC0
.db  0x00, 0x0F, 0x0C, 0xF0, 0xF0, 0xF0, 0xC0, 0x3C, 0x03, 0xF0, 0xFF, 0xC0, 0x0C, 0xC0, 0x00, 0x00
.db  0x00, 0xFF, 0xFC, 0xF0, 0xCF, 0xF0, 0xC0, 0x0C, 0x0C, 0xC0, 0x0C, 0x3C, 0xC0, 0xC3, 0x0C, 0xF0
.db  0x00, 0x30, 0x00, 0xF0, 0xF0, 0xCC, 0xC0, 0x3C, 0x30, 0x0C, 0xF0, 0xF0, 0x00, 0x00, 0x00, 0xFF
.db  0x0C, 0xF8, 0xF8, 0x00, 0x0C, 0xF8, 0x86, 0xF8, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0xF8, 0x0C
.db  0x00, 0xF8, 0x80, 0xF8, 0x80, 0x0C, 0xFC, 0x0C, 0xFC, 0xFC, 0xF8, 0x8C, 0xF8, 0xF8, 0xF8, 0x0C
.db  0xD8, 0xF8, 0xF8, 0x00, 0x0C, 0xF0, 0x8C, 0xF0, 0x0C, 0x0C, 0x1C, 0x0C, 0x0C, 0x0C, 0xF8, 0x0C
.db  0x0C, 0x33, 0xCF, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30, 0x30, 0x00, 0x30, 0x00
.db  0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0x00
.db  0x00, 0xF8, 0x80, 0xF8, 0x80, 0x1C, 0xFC, 0x0C, 0xFC, 0xFC, 0xF8, 0xCC, 0xF8, 0xF8, 0xF8, 0x0C
.db  0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x30, 0x00, 0xC0, 0x80, 0xC0, 0xFC, 0x30, 0x3C, 0x00, 0x00

; Character line 7:
;     0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
.db  0x00, 0xFC, 0xFC, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0xFF, 0x00, 0xFF, 0xC0, 0xC0, 0x00, 0x00, 0xC0
.db  0x00, 0x00, 0xC0, 0x00, 0x00, 0xC0, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0xC0, 0xCC, 0xFC, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30, 0x30, 0x00, 0x30, 0x00
.db  0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.db  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
