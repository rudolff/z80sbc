;================================================================================================
; Convert track/head/sector into LBA for physical access to the disk
;================================================================================================
; DtZ
		
setLBAaddr:	
;		jp qukayamba

; // DtZ
;		call	printInline
;		db	"Reading Track",0

		ifdef	DTZDEBUG

		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		ld	a,(hstdsk)
		call	outbytehex
		ld	a,(hsttrk+1)
		call	outbytehex
		ld	a,(hsttrk)
		call	outbytehex

;		call	printInline
;		db	" ,sector ",0
		ld	a,(hstsec)
		call	outbytehex
;		call	printInline
;		db	",disk ",0
		ld	c,':'
		call	conout
;		call	printInline
;		db	$0D,$0A,0
		endif


		xor	a
		ld	(cf_lba0),a	; lba 0 - always 0
; LBA 1 counting
		ld	a,(hstsec)	; lower bit of track
		add	a,a	; bits 0-5 of lba1
		ld	l,a
		ld	a,(hsttrk)
		and	3	; last two bytes
		add	a,a
		add	a,a
		add	a,a
		add	a,a
		
		add	a,a
		add	a,a	; a = a*40h
		add	a,l
		ld	(cf_lba1),a
; LBA 2 counting
		ld	a,(hsttrk)
		and	$FC	; clearing two less significant bits
		rrc	a
		rrc	a
		ld	l,a	; max 30
		ld	a,(hsttrk+1) ; higher bit of hsttrk - 1 or 0 
		rrc	a		; 70 or 00
		rrc	a
		rrc	a		; should became bit 6 of lba 2 - 40 or 00
		add	a,l	; summing with lower part of track
		ld	l,a
		ld	a,(hstdsk)
		and	1	; lower bit - must became higher bit of lba2
		rrc	a	;********
		add	a,l
		ld	(cf_lba2),a
; LBA 3 counting
		ld	a,(hstdsk)
		and	$FE	; clearing last significant bit
		rrc	a
		ld	(cf_lba3),a
;		call	printInline
;		db	$0D,$0A,"********************LBAS:",0

		ifdef	DTZDEBUG
		ld	a,(cf_lba3)
		call	outbytehex
		ld	a,(cf_lba2)
		call	outbytehex
		ld	a,(cf_lba1)
		call	outbytehex
		ld	a,(cf_lba0)
		call	outbytehex
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif

qukayamba:

		ret
; UNCOMMENT WHEN READY		ret
; ******* READY TO READ//WRITE SECTOR
