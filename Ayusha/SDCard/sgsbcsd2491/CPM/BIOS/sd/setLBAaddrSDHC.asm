;================================================================================================
; Convert track/head/sector into LBA for physical access to the disk
;================================================================================================
; DtZ 20140904

; SDHC
;LBA3 LBA2 
;    LBA1 LBA0
;00H | (000000)DSK DSK |
;    DSK DSK TRKHI TRKLO TRKLO TRKLO TRKLO TRKLO |TRKLO TRKLO TRKLO SEC SEC SEC3 SEC SEC		
setLBAaddr:	
;		jp qukayamba

		ifdef	DTZDEBUG

; // DtZ
;		call	printInline
;		db	"Reading Track",0


		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		ld	a,(hstdsk)
		call	outbytehex
		ld	a,(hsttrk+1)
		call	outbytehex
		ld	a,(hsttrk)
		call	outbytehex

;		call	printInline
;		db	" ,sector ",0
		ld	a,(hstsec)
		call	outbytehex
;		call	printInline
;		db	",disk ",0
		ld	c,':'
		call	conout
;		call	printInline
;		db	$0D,$0A,0
		endif

		ld	a,(hsttrk)
		and	7
		add	a,a
		add	a,a
		add	a,a
		add	a,a
		add	a,a
		ld	l,a
		ld	a,(hstsec)
		add	a,l 
		ld	(cf_lba0),a	; lba0: 0-4 bits - track, 5-7 - lower bits of trklo


		ld	a,(hstdsk)
		and	3
		add	a,a
		ld	l,a
		ld	a,(hsttrk+1) ; only lower bit of it is significant
		and	1
		add	a,l
		add	a,a
		add	a,a
		add	a,a
		add	a,a
		add	a,a
		ld	l,a
		ld	a,(hsttrk)
		and	$F8
		srl	a
		srl	a
		srl	a
		add	a,l
		ld	(cf_lba1),a

		ld	a,(hstdsk)
		and	3		; I bet there is no need
		srl	a
		srl	a
		ld	(cf_lba2),a

		xor	a
		ld	(cf_lba3),a
		
;		call	printInline
;		db	$0D,$0A,"********************LBAS:",0

		ifdef	DTZDEBUG
		ld	a,(cf_lba3)
		call	outbytehex
		ld	a,(cf_lba2)
		call	outbytehex
		ld	a,(cf_lba1)
		call	outbytehex
		ld	a,(cf_lba0)
		call	outbytehex
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif

qukayamba:

		ret
; UNCOMMENT WHEN READY		ret
; ******* READY TO READ//WRITE SECTOR
