
;================================================================================================
; DtZ  Read physical sector from host
;================================================================================================
; Writing 6 byte command
;  Sends a command with parameters = 00 and checksum = $95. Destroys AF.
; Called from sd_init_:

SD_CMD55	equ	$77
SD_CMD10	equ	$4A

sd_write_cmd_with_args:         ; args  bc,de
;        push    de              ; third and forth args Low Looow
;        push    bc              ; arg first and second args Hii Hi

;        push    af
;        call    printInline
;        db      "Writing Command with args: ",0
;        pop     af
;        push    af
;        call    outbytehex
;        call    printInline
;        db $0D,$0A,0
;        pop     af              ; command itself
	push	af
	ld	a,$FF
        call    sd_write_byte
	pop	af

        call    sd_write_byte
;        pop     bc
        ld      a,b             ; b
;        push    bc
        call    sd_write_byte
;        pop     bc
        ld      a,c             ; c
        call    sd_write_byte
;        pop     de
        ld      a,d             ; d
;        push    de
        call    sd_write_byte
;        pop     de
        ld      a,e             ; e
        call    sd_write_byte
        ld      a,$95                   ; $95 is only needed when the CARD INIT is being performed                      
        call    sd_write_byte           ; For the latest command this byte is ignored.
        ret

;********************* READ BYTES FOR NON-FF REPLY *********************/
;sd_wait_for_non_ff:
;        push    bc
;        call    sd_write_cmd_and_get_result_preloop
;        pop     bc
;        ret



sd_wait_for_non_00:
	push 	bc	; this command may take long time
	ld	bc,$FFFF ; 20130204

sd_wait_for_non_00_loop:
	push	bc
	call	sd_read_byte

	ifdef	DTZMINDEBUG
	call	outbytehex
	endif

	and	a
	jr	nz,sd_wait_for_non_00_done	; will jump to sd_wait_non_00_done with 0
	pop	bc
	xor	a
	dec	bc
	or	b
	or	c	; woow 'dec bc' does not set Z flag?
	jr	nz,sd_wait_for_non_00_loop

	ifdef	DTZMINDEBUG
	ld	c,'!'
	call	conout
	ld	c,'W'
	call	conout
	endif

	ld	a,1	; will return 1 on error
	push	bc	; for the next command can pop it
; indicate write error here!
	
sd_wait_for_non_00_done:	
	pop	bc
	pop	bc
	ret


;*********************** SEND COMMAND, WAIT FOR NON-FF  REPLY **************/
sd_write_cmd_with_args_and_get_result:
	call	sd_write_cmd_with_args
sd_write_cmd_and_get_result_preloop:
sd_wait_for_non_ff:
	push	bc
        ld      b,16
sd_write_cmd_and_get_result_loop:       ; Normally this loop executes at least twice,
        push    bc                      ; FF for the first time
;	ld	a,$FE
        call    sd_read_byte
        cp	$FF                               ; still FF?
        jp      nz, sd_write_cmd_and_get_result_done ; No, exiting
        pop     bc
        djnz    sd_write_cmd_and_get_result_loop
	pop	bc
	ret	

sd_write_cmd_and_get_result_done:
        pop     bc              ; remove from stack
	pop	bc
        ret


; Called by sd_write_command
sd_write_byte:
        push    bc

	ifdef	DTZDEBUG
;        push	af
	ld	c,'>'
	call	conout
;	pop	af
	call	outbytehex
	endif

;       push    af
;       call    printInline
;       db      "WB: ",0
;       pop     af
;        push    af

;        call    outbytehex

;        pop     af
        ld      b,8
sd_write_command_bit_loop:
;       push    af
;       ld      a,2
;       out     (SD_PORT),a     ; CS 0 Data 1 Clk 0 , Falling edge  /*** THINK ***/
;       pop     af
        add     a,a
        push    af
        ld      a,0             ; will XOR clear C flag?
        adc     a,0             ; will add C = 1 for bit 1 and 0 for bit 0
        add     a,a             ; if was c -> a==2
        out     (SD_PORT),a   ; Data on data line, Clock low
        inc     a               ; Clk to high
        out     (SD_PORT),a     ; 
;       push    af
;       call    outbytehex
;       pop     af
        dec     a               ; clock to low, data still on data bus, but noone cares! 
        out     (SD_PORT),a
        pop     af
        djnz    sd_write_command_bit_loop
;       call    printInline
;       db      "..Done",$0A,$0D,0
        pop     bc      ; pushed after sd_write_byte
        ret

   ;********************************************* READING BYTE *****************************************
sd_read_byte:
        push    bc
        ld      b,8     ; will read 8 bits
        xor     a       ; Initially we have 0
        ld      c,a     ; we will keep it to c

sd_read_byte_read_bit:

        ld      a,c     ; c = c*2 (or can we rotate C left?)
        add     a,a
        ld      c,a
;	rl	c

        ld      a,3     ; CS= 0, data=1, clock=1
        out     (SD_PORT),a ;Should be read at Falling edge, but this is a rising one? But SPI MODE0 says that's ok
        in      a,(SD_PORT)
;       push    af
;       call    outbytehex
;       pop     af
        and     1       ; only  lower bit is significant
        xor     1       ; our input is inverted
        add     a,c
        ld      c,a

        ld      a,2     ; CS=0, data=1,clock=0
        out     (SD_PORT),a
        djnz    sd_read_byte_read_bit

        ld      a,c     ; helding result in a

	ifdef	DTZDEBUG
	ld	c,'<'
	call	conout
        call    outbytehex
	endif

        pop     bc
        ret

writehst:
;		push	bc
;		push	hl
;		push	af
		call	setLBAaddr
		ld	hl,cf_lba0
		ld	e,(hl)		; lowest byte
		inc	hl
		ld	d,(hl)
		inc	hl
		ld	c,(hl)
		inc	hl
		ld	b,(hl)		; higest byte, lba0
		ld	a,SD_CMD24	; write sector
		call	sd_write_cmd_with_args_and_get_result
		ld	a,$FF		; one byte gap (http://www.retroleum.co.uk/electronics-articles/basic-mmc-card-access/)
		call	sd_write_byte
		ld	a,$FE		; data packet header
		call	sd_write_byte
		
;		call	sd_wait_for_non_ff
		
		ld      bc,$200 ; normally $200 , and then checksumm
		LD 	HL,hstbuf
writehst_loop:
		ld	a,(HL)
		call	sd_write_byte
		inc	hl
		xor	a
		dec	bc
		or	b
		or	c
		jr	nz,writehst_loop

		ifdef	DTZDEBUG
		ld	c,'W'
		call	conout
		ld	c,'d'
		call	conout
		endif

		xor	a
		call    sd_write_byte	; checksumm
		call    sd_write_byte	; checksumm
		
		call	sd_wait_for_non_ff ; Checking for error condition
		and	$E
		cp	4
		jr	z,writehst_ok

		ifdef	DTZDEBUG
		ld	c,'!'
		call	conout
		endif

		ifdef	DTZMINDEBUG
		ld	c,'!'
		call	conout
		ld	c,'w'
		call	conout
		endif

		; Not ok - lets indicate error
		ld	a,1
		jr	writehst_return

writehst_ok:  
		; waiting data to be actually written
		call	sd_wait_for_non_00
		and	a
		jr	nz,writehst_ok_return	; Got not 0 - ok

		ifdef	DTZDEBUG
		ld	c,'!'	; Write error
		call	conout
		ld	c,'W'
		call	conout
		ld	c,'E'
		call	conout
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif


		inc	a	; still zero after all iteration - BAD, indicating error
		jr	writehst_return

		; and it was also a recomendation to verify.. verify... smth.... where? I should find that url...

writehst_ok_return:
		ifdef	DTZDEBUG
		ld	c,'*'
		call	conout
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif
		xor	a		; clearing error flag
writehst_return:
		ld	(erflag),a	; stroring error flag
		ret


readhst:
;		ret
;		push	bc
; ******		push	hl
;		push	af
		call	setLBAaddr

;		jp	readhst_done

		ld	hl,cf_lba0
		ld	e,(hl)		; lowest byte
		inc	hl
		ld	d,(hl)
		inc	hl
		ld	c,(hl)
		inc	hl
		ld	b,(hl)		; higest byte, lba0
;		ld	a,SD_CMD17	; read sector
;		call	sd_write_cmd_with_args_and_get_result
;		call    sd_wait_for_non_ff
;		cp      $FE
;		jp      nz, readhst_cannot_read_block		

		LD 	HL,hstbuf

readhst_plain:
		push	hl
		ld	a,SD_CMD17
		call	sd_write_cmd_with_args_and_get_result
		call    sd_wait_for_non_ff

;		ld	b,18
;cmd10loop:
;		call	sd_read_byte
;		djnz	cmd10loop
;		jp	readhst_done


;		LD 	HL,hstbuf
;		ld	a,h
;		call	outbytehex
;		ld	a,l
;		call	outbytehex
;		ld	c,'#'
;		call	conout
		ld      bc,$200 ; normally $200 , and then read a CRC
;		jp	readhst_done
		pop	hl
		
readhst_readblockloop:
		push	bc

		call	sd_read_byte
		ld	(hl),a

		inc	hl
		pop	bc

		dec     bc
		xor     a
		or      b
		or	c
		jr      nz,readhst_readblockloop

		ifdef	DTZDEBUG
		ld	c,'R'
		call	conout
		ld	c,'d'
		call	conout
		endif
		

		call    sd_read_byte	; reading//ignoring checksumm
		call    sd_read_byte

		ifdef	DTZDEBUG
		ld	c,'*'
		call	conout
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif
		
;		call	printInline
;		db	$0D,$0A,"Block read",$0D,$0A,0
		jr	readhst_done


readhst_cannot_read_block:
;		pop	af
		ld	a,1	; Error
		jr	readhst_done_do_not_clear
readhst_done:
;		pop	af
		xor	a
readhst_done_do_not_clear:
;		pop	hl
;		pop	bc
		ld	(erflag),a	; No Error
		RET


