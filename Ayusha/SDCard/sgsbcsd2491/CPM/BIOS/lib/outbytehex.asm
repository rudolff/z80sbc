    ifdef	DTZDEBUG 
INCLUDEOUTBYTEHEX	EQU	1
    endif
    ifdef DTZMINDEBUG
INCLUDEOUTBYTEHEX	EQU	1
    endif
    ifdef INCLUDEOUTBYTEHEX

outbytehex:	;does not spoils af
  push	bc
  push	af
  and	$f0
  rra
  rra
  rra
  rra
  call	outnibble
  pop	af
  push	af
  and	$0f
  call	outnibble
  pop	af
  pop	bc
  ret

outnibble:		;spoils	af,bc
  and	$0f
  cp	$0a
  jp	nc,outnibbleless
  add	a,48
  ld	c,a
  call	conout
  ret
outnibbleless:
  add	a,'A'-10	;	A - 10
  ld	c,a
  call	conout
  ret

    endif
  