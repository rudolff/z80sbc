;
; usage:
;	call	outstringinline
;	db	"Sometext",0
; warning:	spoils	hl, c


outstringinline:
    pop	hl			; read pc from stack
    call	outstring
    inc	hl
    push	hl
    ret
    
; usage:	hl - asciiz

outstring:
;    push	bc
outstringloop:
    ld	a,(hl)
    cp	0
    jp	nz,outstringoutchar
;    pop	bc
    ret
outstringoutchar:
    ld	c,a
    call	conout
    inc	hl
    jp outstringloop
    