; Contents of this file are copyright Grant Searle
; Blocking/unblocking routines are the published version by Digital Research
; (bugfixed, as found on the web)
;
; You have permission to use this for NON COMMERCIAL USE ONLY
; If you wish to use it elsewhere, please include an acknowledgement to myself.
;
; http://home.micros.users.btopenworld.com/cpm/index.htm
;
; eMail: home.micros01@btinternet.com
;
; If the above don't work, please perform an Internet search to see if I have
; updated the web page hosting service.
;
;==================================================================================

; DtZ
;ccp		equ	0C000h		; Base of CCP.
ccp		equ	DTZTPA		; Base of CCP.
; was ccp		equ	0D000h		; Base of CCP.
; Defined in Makefile
;
;
ccpsectoroffset		equ 	DTZSTARTSECTOR	; first sector ccp resides
; for C000, that should be 0
; -//- C200 - 2 (+512bytes, 1 SD sector)
; -//- C400 - 4 
; C800 - 8
; D000 - 10 

; // DtZ

bdos		equ	ccp + 0806h	; Base of BDOS.
bios		equ	ccp + 1600h	; Base of BIOS.

; Set CP/M low memory datA, vector and buffer addresses.

iobyte		equ	03h		; Intel standard I/O definition byte.
userdrv		equ	04h		; Current user number and drive.
tpabuf		equ	80h		; Default I/O buffer and command line storage.


SER_BUFSIZE	equ	60
SER_FULLSIZE	equ	20
SER_EMPTYSIZE	equ	5


RTS_HIGH	equ	0E8H
RTS_LOW		equ	0EAH

SIOA_D		equ	$00
SIOA_C		equ	$02
SIOB_D		equ	$01
SIOB_C		equ	$03

int38		equ	38H
nmi		equ	66H

blksiz		equ	4096		;CP/M allocation size
hstsiz		equ	512		;host disk sector size
hstspt		equ	32		;host disk sectors/trk
hstblk		equ	hstsiz/128	;CP/M sects/host buff
cpmspt		equ	hstblk * hstspt	;CP/M sectors/track
secmsk		equ	hstblk-1	;sector mask
					;compute sector mask
;secshf		equ	2		;log2(hstblk)

wrall		equ	0		;write to allocated
wrdir		equ	1		;write to directory
wrual		equ	2		;write to unallocated

; DtZ
; SD port
SD_PORT		equ	$8
SD_CMD17	equ	$51		; read single block
SD_CMD24	equ	$58

; CF registers
;CF_DATA		equ	$10
;CF_FEATURES	equ	$11
;CF_ERROR	equ	$11
;CF_SECCOUNT	equ	$12
;CF_SECTOR	equ	$13
;CF_CYL_LOW	equ	$14
;CF_CYL_HI	equ	$15
;CF_HEAD		equ	$16
;CF_STATUS	equ	$17
;CF_COMMAND	equ	$17
;CF_LBA0		equ	$13
;CF_LBA1		equ	$14
;CF_LBA2		equ	$15
;CF_LBA3		equ	$16

;CF Features
;CF_8BIT		equ	1
;CF_NOCACHE	equ	082H
;CF Commands
;CF_READ_SEC	equ	020H
;CF_WRITE_SEC	equ	030H
;CF_SET_FEAT	equ 	0EFH

; / DtZ

LF		equ	0AH		;line feed
FF		equ	0CH		;form feed
CR		equ	0DH		;carriage RETurn

;================================================================================================

		org	bios		; BIOS origin.

;================================================================================================
; BIOS jump table.
;================================================================================================
		JP	boot		;  0 Initialize.
wboote:		JP	wboot		;  1 Warm boot.
		JP	const		;  2 Console status.
		JP	conin		;  3 Console input.
		JP	conout		;  4 Console OUTput.
		JP	list		;  5 List OUTput.
		JP	punch		;  6 punch OUTput.
		JP	reader		;  7 Reader input.
		JP	home		;  8 Home disk.
		JP	seldsk		;  9 Select disk.
		JP	settrk		; 10 Select track.
		JP	setsec		; 11 Select sector.
		JP	setdma		; 12 Set DMA ADDress.
		JP	read		; 13 Read 128 bytes.
		JP	write		; 14 Write 128 bytes.
		JP	listst		; 15 List status.
		JP	sectran		; 16 Sector translate.
		ifdef	DTZDISKMAP
; DtZ extension: diskmap
		JP	diskmap
		endif

;================================================================================================
; Disk parameter headers for disk 0 to 15
;================================================================================================
dpbase:
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb0,0000h,alv00
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv01
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv02
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv03
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv04
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv05
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv06
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv07
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv08
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv09
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv10
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv11
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv12
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv13
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpb,0000h,alv14
	 	dw 0000h,0000h,0000h,0000h,dirbuf,dpbLast,0000h,alv15

; First drive has a reserved track for CP/M
dpb0:
		dw 128 ;SPT - sectors per track
		db 5   ;BSH - block shift factor
		db 31  ;BLM - block mask
		db 1   ;EXM - Extent mask
		dw 2043 ; (2047-4) DSM - Storage size (blocks - 1)
		dw 511 ;DRM - Number of directory entries - 1
		db 240 ;AL0 - 1 bit set per directory block
		db 0   ;AL1 -            "
		dw 0   ;CKS - DIR check vector size (DRM+1)/4 (0=fixed disk)
		dw 1   ;OFF - Reserved tracks

dpb:
		dw 128 ;SPT - sectors per track
		db 5   ;BSH - block shift factor
		db 31  ;BLM - block mask
		db 1   ;EXM - Extent mask
		dw 2047 ;DSM - Storage size (blocks - 1)
		dw 511 ;DRM - Number of directory entries - 1
		db 240 ;AL0 - 1 bit set per directory block
		db 0   ;AL1 -            "
		dw 0   ;CKS - DIR check vector size (DRM+1)/4 (0=fixed disk)
		dw 0   ;OFF - Reserved tracks

; Last drive is smaller because CF is never full 64MB or 128MB
dpbLast:
		dw 128 ;SPT - sectors per track
		db 5   ;BSH - block shift factor
		db 31  ;BLM - block mask
		db 1   ;EXM - Extent mask
		dw 511 ;DSM - Storage size (blocks - 1)  ; 511 = 2MB (for 128MB card), 1279 = 5MB (for 64MB card)
		dw 511 ;DRM - Number of directory entries - 1
		db 240 ;AL0 - 1 bit set per directory block
		db 0   ;AL1 -            "
		dw 0   ;CKS - DIR check vector size (DRM+1)/4 (0=fixed disk)
		dw 0   ;OFF - Reserved tracks

;================================================================================================
; Cold boot
;================================================================================================

boot:
		DI				; Disable interrupts.
		LD	SP,biosstack		; Set default stack.

;		Turn off ROM

		LD	A,$01
		OUT ($38),A

;	Initialise SIO

		LD	A,$00
		OUT	(SIOA_C),A
		LD	A,$18
		OUT	(SIOA_C),A

		LD	A,$04
		OUT	(SIOA_C),A
		LD	A,$C4
		OUT	(SIOA_C),A

		LD	A,$01
		OUT	(SIOA_C),A
		LD	A,$18
		OUT	(SIOA_C),A
	
		LD	A,$03
		OUT	(SIOA_C),A
		LD	A,$E1
		OUT	(SIOA_C),A

		LD	A,$05
		OUT	(SIOA_C),A
		LD	A,RTS_LOW
		OUT	(SIOA_C),A

		LD	A,$00
		OUT	(SIOB_C),A
		LD	A,$18
		OUT	(SIOB_C),A

		LD	A,$04
		OUT	(SIOB_C),A
		LD	A,$C4
		OUT	(SIOB_C),A

		LD	A,$01
		OUT	(SIOB_C),A
		LD	A,$18
		OUT	(SIOB_C),A

		LD	A,$02
		OUT	(SIOB_C),A
		LD	A,$E0		; INTERRUPT VECTOR ADDRESS
		OUT	(SIOB_C),A
	
		LD	A,$03
		OUT	(SIOB_C),A
		LD	A,$E1
		OUT	(SIOB_C),A

		LD	A,$05
		OUT	(SIOB_C),A
		LD	A,RTS_LOW
		OUT	(SIOB_C),A

		; Interrupt vector in page FF
		LD	A,$FF
		LD	I,A

		CALL	printInline
		db FF
		db "Z80 CP/M BIOS 1.0 by G. Searle 2007-13/DtZ "
		include <../../version.asm>
		ifdef	DTZSDHC
		db	"SDHC"
		endif
		ifdef	DTZDEBUG
		db	"D"
		endif
		db CR,LF
		db CR,LF
		db "CP/M 2.2 "
		db	"Copyright"
		db	" 1979 (c) by Digital Research"
		db CR,LF,0


		LD	A,(iobyteo)			; Set iobyte as it was set by ROM
		LD	(iobyte),A			;
; DEBUGG
;		ADD	A,48
;		LD	C,A
;		CALL	conout
;		CALL	printInline
;		db	"<--IOBYTE",CR,LF,0


		XOR	A
		LD	(userdrv),A			; Clear drive bytes

		LD	(serABufUsed),A
		LD	(serBBufUsed),A
		LD	HL,serABuf
		LD	(serAInPtr),HL
		LD	(serARdPtr),HL

		LD	HL,serBBuf
		LD	(serBInPtr),HL
		LD	(serBRdPtr),HL

		JP	gocpm

;================================================================================================
; Warm boot
;================================================================================================

wboot:
		ifdef	DTZDEBUG
		ld	c,'*'
		call	conout
		ld	c,'B'
		call	conout
		ld	c,'o'
		call	conout
		ld	c,'*'
		call	conout
		ld	c,CR
		call	conout
		ld	c,LF
		call	conout
		endif
		
		DI				; Disable interrupts.
		LD	SP,biosstack		; Set default stack.



		; Interrupt vector in page FF
		LD	A,$FF
		LD	I,A


		ifndef	DTZSDHC

		ld	b,11			; number of sectors to reload
		ld	d,ccpsectoroffset ; ie in CE00 stystem first lba is
		ld	hl,ccp			  ; 00 00 0E 00
wboot_sd_loop:
		push	bc
		ld	e,0
		push	de
		ld	b,e
		ld	c,e	; track 0 of startspace + ccpsectoroffset
		call	readhst_plain
		pop 	de
		inc 	d
		inc 	d	; de = de+512
		pop 	bc
		djnz	wboot_sd_loop
		endif

		ifdef	DTZSDHC	
		ld	b,11	; number of sectors to reload
		ld	e,ccpsectoroffset/2 ; So the first LBA is 00 00 00 07
		ld	d,0
		ld	hl,ccp
wboot_sd_loop:
		push	bc
		ld	b,d	; BC = 0
		ld	c,d
		push	de
		call	readhst_plain
		pop		de
		inc	e
		pop	bc
		djnz	wboot_sd_loop
		endif
		
		



		IFDEF	CF_STUFF_NOT_TO_USER
		LD	B,11 ; Number of sectors to reload

		LD	A,0
		LD	(hstsec),A

		LD	HL,ccp
rdSectors:

		LD	A,(hstsec)
		OUT 	(CF_LBA0),A
		LD	A,0
		OUT 	(CF_LBA1),A
		OUT 	(CF_LBA2),A
		LD	a,0E0H
		OUT 	(CF_LBA3),A
		LD 	A,1
		OUT 	(CF_SECCOUNT),A

		PUSH 	BC


		LD 	A,CF_READ_SEC
		OUT 	(CF_COMMAND),A


		LD 	c,4
rd4secs512:
		LD 	b,128
rdByte512:
		in 	A,(CF_DATA)
		LD 	(HL),A
		iNC 	HL
		dec 	b
		JR 	NZ, rdByte512
		dec 	c
		JR 	NZ,rd4secs512

		POP 	BC

		LD	A,(hstsec)
		INC	A
		LD	(hstsec),A

		djnz	rdSectors

		ENDIF	; CF_STUFF_NOT_TO_USER

;================================================================================================
; Common code for cold and warm boot
;================================================================================================

gocpm:
		xor	a			;0 to accumulator
		ld	(hstact),a		;host buffer inactive
		ld	(unacnt),a		;clear unalloc count

		LD	HL,serialInt		; ADDress of serial interrupt.
		LD	($40),HL

		LD	HL,tpabuf		; ADDress of BIOS DMA buffer.
		LD	(dmaAddr),HL
		LD	A,0C3h			; Opcode for 'JP'.
		LD	(00h),A			; Load at start of RAM.
		LD	HL,wboote		; ADDress of jump for a warm boot.
		LD	(01h),HL
		LD	(05h),A			; Opcode for 'JP'.
		LD	HL,bdos			; ADDress of jump for the BDOS.
		LD	(06h),HL
		LD	A,(userdrv)		; Save new drive number (0).
		LD	c,A			; Pass drive number in C.

		IM	2
		EI				; Enable interrupts

		JP	ccp			; Start CP/M by jumping to the CCP.

;================================================================================================
; Console I/O routines
;================================================================================================

serialInt:	PUSH	AF
		PUSH	HL

		; Check if there is a char in channel A
		; If not, there is a char in channel B
		SUB	A
		OUT 	(SIOA_C),A
		IN   	A,(SIOA_C)	; Status byte D2=TX Buff Empty, D0=RX char ready	
		RRCA			; Rotates RX status into Carry Flag,	
		JR	NC, serialIntB

serialIntA:
		LD	HL,(serAInPtr)
		INC	HL
		LD	A,L
serABufSER_BUFSIZE_and_ff EQU (serABuf+SER_BUFSIZE) & $FF
		CP	serABufSER_BUFSIZE_and_ff
		JR	NZ, notAWrap
		LD	HL,serABuf
notAWrap:
		LD	(serAInPtr),HL
		IN	A,(SIOA_D)
		LD	(HL),A

		LD	A,(serABufUsed)
		INC	A
		LD	(serABufUsed),A
		CP	SER_FULLSIZE
		JR	C,rtsA0
	        LD   	A,$05
		OUT  	(SIOA_C),A
	        LD   	A,RTS_HIGH
		OUT  	(SIOA_C),A
rtsA0:
		POP	HL
		POP	AF
		EI
		RETI

serialIntB:
		LD	HL,(serBInPtr)
		INC	HL
		LD	A,L
serBBuf_SER_BUFSIZE_and_FF	equ	(serBBuf+SER_BUFSIZE) & $FF
		CP	serBBuf_SER_BUFSIZE_and_FF
		JR	NZ, notBWrap
		LD	HL,serBBuf
notBWrap:
		LD	(serBInPtr),HL
		IN	A,(SIOB_D)
		LD	(HL),A

		LD	A,(serBBufUsed)
		INC	A
		LD	(serBBufUsed),A
		CP	SER_FULLSIZE
		JR	C,rtsB0
	        LD   	A,$05
		OUT  	(SIOB_C),A
	        LD   	A,RTS_HIGH
		OUT  	(SIOB_C),A
rtsB0:
		POP	HL
		POP	AF
		EI
		RETI

;------------------------------------------------------------------------------------------------

const:
		LD	A,(iobyte)
		AND	00001011b ; Mask off console and high bit of reader
		CP	00001010b ; redirected to reader on UR1/2 (Serial A)
		JR	Z,constA
		CP	00000010b ; redirected to reader on TTY/RDR (Serial B)
		JR	Z,constB

		AND	$03 ; remove the reader from the mask - only console bits then remain
		CP	$01
		JR	NZ,constB
constA:
		PUSH	HL
		LD	A,(serABufUsed)
		CP	$00
		JR	Z, dataAEmpty
 		LD	A,0FFH
		POP	HL
		RET
dataAEmpty:
		LD	A,0
		POP	HL
        	RET





constB:
		PUSH	HL
		LD	A,(serBBufUsed)
		CP	$00
		JR	Z, dataBEmpty
 		LD	A,0FFH
		POP	HL
		RET
dataBEmpty:
		LD	A,0
		POP	HL
        	RET
;------------------------------------------------------------------------------------------------

reader:		
		PUSH	HL
		PUSH	AF
reader2:	LD	A,(iobyte)
		AND	$08
		CP	$08
		JR	NZ,coninB
		JR	coninA

conin:
		PUSH	HL
		PUSH	AF
		LD	A,(iobyte)
		AND	$03
		CP	$02
		JR	Z,reader2	; "BAT:" redirect
		CP	$01
		JR	NZ,coninB
		
;------------------------------------------------------------------------------------------------

coninA:
		POP	AF
waitForCharA:
		LD	A,(serABufUsed)
		CP	$00
		JR	Z, waitForCharA
		LD	HL,(serARdPtr)
		INC	HL
		LD	A,L
serABuf_SER_BUFSIZE_and_FF EQU  (serABuf+SER_BUFSIZE) & $FF
        CP      serABuf_SER_BUFSIZE_and_FF
		JR	NZ, notRdWrapA
		LD	HL,serABuf
notRdWrapA:
		DI
		LD	(serARdPtr),HL

		LD	A,(serABufUsed)
		DEC	A
		LD	(serABufUsed),A

		CP	SER_EMPTYSIZE
		JR	NC,rtsA1
	        LD   	A,$05
		OUT  	(SIOA_C),A
	        LD   	A,RTS_LOW
		OUT  	(SIOA_C),A
rtsA1:
		LD	A,(HL)
		EI

		POP	HL

		RET			; Char ready in A


coninB:
		POP	AF
waitForCharB:
		LD	A,(serBBufUsed)
		CP	$00
		JR	Z, waitForCharB
		LD	HL,(serBRdPtr)
		INC	HL
		LD	A,L
serBBuf_SER_BUFSIZE_and_FF              equ     (serBBuf+SER_BUFSIZE) & $FF
        CP      serBBuf_SER_BUFSIZE_and_FF
		JR	NZ, notRdWrapB
		LD	HL,serBBuf
notRdWrapB:
		DI
		LD	(serBRdPtr),HL

		LD	A,(serBBufUsed)
		DEC	A
		LD	(serBBufUsed),A

		CP	SER_EMPTYSIZE
		JR	NC,rtsB1
	        LD   	A,$05
		OUT  	(SIOB_C),A
	        LD   	A,RTS_LOW
		OUT  	(SIOB_C),A
rtsB1:
		LD	A,(HL)
		EI

		POP	HL

		RET			; Char ready in A
;------------------------------------------------------------------------------------------------
list:		PUSH	AF		; Store character
list2:		LD	A,(iobyte)
		AND	$C0
		CP	$40
		JR	NZ,conoutB1
		JR	conoutA1

;------------------------------------------------------------------------------------------------
punch:		PUSH	AF		; Store character
		LD	A,(iobyte)
		AND	$20
		CP	$20
		JR	NZ,conoutB1
		JR	conoutA1

;------------------------------------------------------------------------------------------------
conout:		PUSH	AF		; Store character
		LD	A,(iobyte)
		AND	$03
		CP	$02
		JR	Z,list2		; "BAT:" redirect
		CP	$01
		JR	NZ,conoutB1

conoutA1:	CALL	CKSIOA		; See if SIO channel B is finished transmitting
		JR	Z,conoutA1	; Loop until SIO flag signals ready
		LD	A,C
		OUT	(SIOA_D),A		; OUTput the character
		POP	AF		; RETrieve character
		RET

conoutB1:	CALL	CKSIOB		; See if SIO channel B is finished transmitting
		JR	Z,conoutB1	; Loop until SIO flag signals ready
		LD	A,C
		OUT	(SIOB_D),A		; OUTput the character
		POP	AF		; RETrieve character
		RET

;------------------------------------------------------------------------------------------------
CKSIOA
		SUB	A
		OUT 	(SIOA_C),A
		IN   	A,(SIOA_C)	; Status byte D2=TX Buff Empty, D0=RX char ready	
		RRCA			; Rotates RX status into Carry Flag,	
		BIT  	1,A		; Set Zero flag if still transmitting character	
        	RET

CKSIOB
		SUB	A
		OUT 	(SIOB_C),A
		IN   	A,(SIOB_C)	; Status byte D2=TX Buff Empty, D0=RX char ready	
		RRCA			; Rotates RX status into Carry Flag,	
		BIT  	1,A		; Set Zero flag if still transmitting character	
        	RET

;------------------------------------------------------------------------------------------------
listst:		LD	A,$FF		; Return list status of 0xFF (ready).
		RET

;================================================================================================
; Disk processing entry points
;================================================================================================

seldsk:
		LD	HL,$0000
		LD	A,C
		CP	16		; 16 for 128MB disk, 8 for 64MB disk
		jr	C,chgdsk	; if invalid drive will give BDOS error
		LD	A,(userdrv)	; so set the drive back to a:
		CP	C		; If the default disk is not the same as the
		RET	NZ		; selected drive then return, 
		XOR	A		; else reset default back to a:
		LD	(userdrv),A	; otherwise will be stuck in a loop
		LD	(sekdsk),A
		ret

chgdsk:		LD 	(sekdsk),A
		RLC	a		;*2
		RLC	a		;*4
		RLC	a		;*8
		RLC	a		;*16
		LD 	HL,dpbase
		LD	b,0
		LD	c,A	
		ADD	HL,BC

		RET


		ifdef	DTZDISKMAP
;Extension: report/set disk mapping
; A - disk number, A: is 0
; E = 0 - read current disk map; returns into E
; E = 1 - set new disk mapping in C (?)
; Returns A=1 for error
; 
;		   A:B:C:D:E:F:G:H:I:J:K: L: M: N: O: P:
diskmapT:	db 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
diskmape:	LD	A,1		; We have error
		RET
diskmap:	CP	14		; Disk 15 has non standart geometry in searle grant's defines
		JR	NC,diskmape
		AND	A
		JR	Z,diskmape	; Disk 0 has system area, do not allow to map it
		LD	HL,diskmapT
		LD	E,A
		LD	D,0
		ADD	HL,DE		; HL holds current mapping for requested disk
		LD	A,E
		AND	A
		JR	Z,diskmapr	; disk map read
		LD	(HL),C		; setting disk map
		XOR	A
		RET
diskmapr:	LD	E,(HL)
		XOR	A
		RET
		endif

;------------------------------------------------------------------------------------------------
home:
		ld	a,(hstwrt)	;check for pending write
		or	a
		jr	nz,homed
		ld	(hstact),a	;clear host active flag
homed:
		LD 	BC,0000h

;------------------------------------------------------------------------------------------------
settrk:		LD 	(sektrk),BC	; Set track passed from BDOS in register BC.
		RET

;------------------------------------------------------------------------------------------------
setsec:		LD 	(seksec),BC	; Set sector passed from BDOS in register BC.
		RET

;------------------------------------------------------------------------------------------------
setdma:		LD 	(dmaAddr),BC	; Set DMA ADDress given by registers BC.
		RET

;------------------------------------------------------------------------------------------------
sectran:	PUSH 	BC
		POP 	HL
		RET

;------------------------------------------------------------------------------------------------
read:
		;read the selected CP/M sector
		xor	a
		ld	(unacnt),a
		ld	a,1
		ld	(readop),a		;read operation
		ld	(rsflag),a		;must read data
		ld	a,wrual
		ld	(wrtype),a		;treat as unalloc
		jp	rwoper			;to perform the read


;------------------------------------------------------------------------------------------------
write:
		;write the selected CP/M sector
		xor	a		;0 to accumulator
		ld	(readop),a	;not a read operation
		ld	a,c		;write type in c
		ld	(wrtype),a
		cp	wrual		;write unallocated?
		jr	nz,chkuna	;check for unalloc
;
;		write to unallocated, set parameters
		ld	a,blksiz/128	;next unalloc recs
		ld	(unacnt),a
		ld	a,(sekdsk)		;disk to seek
		ld	(unadsk),a		;unadsk = sekdsk
		ld	hl,(sektrk)
		ld	(unatrk),hl		;unatrk = sectrk
		ld	a,(seksec)
		ld	(unasec),a		;unasec = seksec
;
chkuna:
;		check for write to unallocated sector
		ld	a,(unacnt)		;any unalloc remain?
		or	a	
		jr	z,alloc		;skip if not
;
;		more unallocated records remain
		dec	a		;unacnt = unacnt-1
		ld	(unacnt),a
		ld	a,(sekdsk)		;same disk?
		ld	hl,unadsk
		cp	(hl)		;sekdsk = unadsk?
		jp	nz,alloc		;skip if not
;
;		disks are the same
		ld	hl,unatrk
		call	sektrkcmp	;sektrk = unatrk?
		jp	nz,alloc		;skip if not
;
;		tracks are the same
		ld	a,(seksec)		;same sector?
		ld	hl,unasec
		cp	(hl)		;seksec = unasec?
		jp	nz,alloc		;skip if not
;
;		match, move to next sector for future ref
		inc	(hl)		;unasec = unasec+1
		ld	a,(hl)		;end of track?
		cp	cpmspt		;count CP/M sectors
		jr	c,noovf		;skip if no overflow
;
;		overflow to next track
		ld	(hl),0		;unasec = 0
		ld	hl,(unatrk)
		inc	hl
		ld	(unatrk),hl		;unatrk = unatrk+1
;
noovf:
		;match found, mark as unnecessary read
		xor	a		;0 to accumulator
		ld	(rsflag),a		;rsflag = 0
		jr	rwoper		;to perform the write
;
alloc:
		;not an unallocated record, requires pre-read
		xor	a		;0 to accum
		ld	(unacnt),a		;unacnt = 0
		inc	a		;1 to accum
		ld	(rsflag),a		;rsflag = 1

;------------------------------------------------------------------------------------------------
rwoper:
		;enter here to perform the read/write
		xor	a		;zero to accum
		ld	(erflag),a		;no errors (yet)
		ld	a,(seksec)		;compute host sector
		or	a		;carry = 0
		rra			;shift right
		or	a		;carry = 0
		rra			;shift right
		ld	(sekhst),a		;host sector to seek
;
;		active host sector?
		ld	hl,hstact	;host active flag
		ld	a,(hl)
		ld	(hl),1		;always becomes 1
		or	a		;was it already?
		jr	z,filhst		;fill host if not
;
;		host buffer active, same as seek buffer?
		ld	a,(sekdsk)
		ld	hl,hstdsk	;same disk?
		cp	(hl)		;sekdsk = hstdsk?
		jr	nz,nomatch
;
;		same disk, same track?
		ld	hl,hsttrk
		call	sektrkcmp	;sektrk = hsttrk?
		jr	nz,nomatch
;
;		same disk, same track, same buffer?
		ld	a,(sekhst)
		ld	hl,hstsec	;sekhst = hstsec?
		cp	(hl)
		jr	z,match		;skip if match
;
nomatch:
		;proper disk, but not correct sector
		ld	a,(hstwrt)		;host written?
		or	a
		call	nz,writehst	;clear host buff
;
filhst:
		;may have to fill the host buffer
		ld	a,(sekdsk)
		ld	(hstdsk),a
		ld	hl,(sektrk)
		ld	(hsttrk),hl
		ld	a,(sekhst)
		ld	(hstsec),a
		ld	a,(rsflag)		;need to read?
		or	a
		call	nz,readhst		;yes, if 1
		xor	a		;0 to accum
		ld	(hstwrt),a		;no pending write
;
match:
		;copy data to or from buffer
		ld	a,(seksec)		;mask buffer number
		and	secmsk		;least signif bits
		ld	l,a		;ready to shift
		ld	h,0		;double count
		add	hl,hl
		add	hl,hl
		add	hl,hl
		add	hl,hl
		add	hl,hl
		add	hl,hl
		add	hl,hl
;		hl has relative host buffer address
		ld	de,hstbuf
		add	hl,de		;hl = host address
		ex	de,hl			;now in DE
		ld	hl,(dmaAddr)		;get/put CP/M data
		ld	c,128		;length of move
		ld	a,(readop)		;which way?
		or	a
		jr	nz,rwmove		;skip if read
;
;	write operation, mark and switch direction
		ld	a,1
		ld	(hstwrt),a		;hstwrt = 1
		ex	de,hl			;source/dest swap
;
rwmove:
		;C initially 128, DE is source, HL is dest
		ld	a,(de)		;source character
		inc	de
		ld	(hl),a		;to dest
		inc	hl
		dec	c		;loop 128 times
		jr	nz,rwmove
;
;		data has been moved to/from host buffer
		ld	a,(wrtype)		;write type
		cp	wrdir		;to directory?
		ld	a,(erflag)		;in case of errors
		ret	nz			;no further processing
;
;		clear host buffer for directory write
		or	a		;errors?
		ret	nz			;skip if so
		xor	a		;0 to accum
		ld	(hstwrt),a		;buffer written
		call	writehst
		ld	a,(erflag)
		ret

;------------------------------------------------------------------------------------------------
;Utility subroutine for 16-bit compare
sektrkcmp:
		;HL = .unatrk or .hsttrk, compare with sektrk
		ex	de,hl
		ld	hl,sektrk
		ld	a,(de)		;low byte compare
		cp	(HL)		;same?
		ret	nz			;return if not
;		low bytes equal, test high 1s
		inc	de
		inc	hl
		ld	a,(de)
		cp	(hl)	;sets flags
		ret

;================================================================================================
; Convert track/head/sector into LBA for physical access to the disk
;================================================================================================
;setLBAaddr:	
		ifdef	DTZSDHC
		include	<setLBAaddrSDHC.asm>
		endif
		ifndef	DTZSDHC
		include	<setLBAaddr.asm>
		endif


;		include	<setLBAgrant.asm>

;		RET				

;================================================================================================
; Read physical sector from host
;================================================================================================

		include	<sdlib.asm>
		
;================================================================================================
; Write physical sector to host
;================================================================================================

;writehst:
;		RET
;		PUSH 	AF
;		PUSH 	BC
;		PUSH 	HL


;		CALL 	cfWait

;		CALL 	setLBAaddr

;		LD 	A,CF_WRITE_SEC
;		OUT 	(CF_COMMAND),A

;		CALL 	cfWait

;		LD 	c,4
;		LD 	HL,hstbuf
;wr4secs:
;		LD 	b,128
;wrByte:		LD 	A,(HL)
;		OUT 	(CF_DATA),A
;		iNC 	HL
;		dec 	b
;		JR 	NZ, wrByte
;
;		dec 	c
;		JR 	NZ,wr4secs
;
;		POP 	HL
;		POP 	BC
;		POP 	AF
;
;		XOR 	a
;		ld	(erflag),a
;		RET

;================================================================================================
; Wait for disk to be ready (busy=0,ready=1)
;================================================================================================
cfWait:
; DtZ
		ret
;		PUSH 	AF
;cfWait1:
;		in 	A,(CF_STATUS)
;		AND 	080H
;		cp 	080H
;		JR	Z,cfWait1
;		POP 	AF
;		RET
; // DtZ

;================================================================================================
; Utilities
;================================================================================================

printInline:
		EX 	(SP),HL 	; PUSH HL and put RET ADDress into HL
		PUSH 	AF
		PUSH 	BC
nextILChar:	LD 	A,(HL)
		CP	0
		JR	Z,endOfPrint
		LD  	C,A
		CALL 	conout		; Print to TTY
		iNC 	HL
		JR	nextILChar
endOfPrint:	INC 	HL 		; Get past "null" terminator
		POP 	BC
		POP 	AF
		EX 	(SP),HL 	; PUSH new RET ADDress on stack and restore HL
		RET
; DtZ
		include	<outbytehex.asm>
;		include	<outstring.asm>
; // DtZ
;================================================================================================
; Data storage
;================================================================================================

dirbuf: 	ds 128 		;scratch directory area
alv00: 		ds 257			;allocation vector 0
alv01: 		ds 257			;allocation vector 1
alv02: 		ds 257			;allocation vector 2
alv03: 		ds 257			;allocation vector 3
alv04: 		ds 257			;allocation vector 4
alv05: 		ds 257			;allocation vector 5
alv06: 		ds 257			;allocation vector 6
alv07: 		ds 257			;allocation vector 7
alv08: 		ds 257			;allocation vector 8
alv09: 		ds 257			;allocation vector 9
alv10: 		ds 257			;allocation vector 10
alv11: 		ds 257			;allocation vector 11
alv12: 		ds 257			;allocation vector 12
alv13: 		ds 257			;allocation vector 13
alv14: 		ds 257			;allocation vector 14
alv15: 		ds 257			;allocation vector 15

;lba0		db	00h
;lba1		db	00h
;lba2		db	00h
;lba3		db	00h
; DtZ
; IOByte as returned by ROM
iobyteo		db	0

cf_lba0		db	00h
cf_lba1		db	00h
cf_lba2		db	00h
cf_lba3		db	00h
; //DtZ
;		ds	080h		; Start of BIOS stack area.
;DtZ
		ds	020h		; Start of BIOS stack area.
biosstack:	equ	$


sekdsk:		ds	1		;seek disk number
sektrk:		ds	2		;seek track number
seksec:		ds	2		;seek sector number
;
hstdsk:		ds	1		;host disk number
hsttrk:		ds	2		;host track number
hstsec:		ds	1		;host sector number
;
sekhst:		ds	1		;seek shr secshf
hstact:		ds	1		;host active flag
hstwrt:		ds	1		;host written flag
;
unacnt:		ds	1		;unalloc rec cnt
unadsk:		ds	1		;last unalloc disk
unatrk:		ds	2		;last unalloc track
unasec:		ds	1		;last unalloc sector
;
erflag:		ds	1		;error reporting
rsflag:		ds	1		;read sector flag
readop:		ds	1		;1 if read operation
wrtype:		ds	1		;write operation type
dmaAddr:	ds	2		;last dma address
hstbuf:		ds	512		;host buffer

hstBufEnd:	equ	$

; v1.0		org	0FF00H
; v1.0 primaryIO	db	00H
serABuf:	ds	SER_BUFSIZE	; SIO A Serial buffer
serAInPtr	dw	00h
serARdPtr	dw	00h
serABufUsed	db	00h
serBBuf:	ds	SER_BUFSIZE	; SIO B Serial buffer
serBInPtr	dw	00h
serBRdPtr	dw	00h
serBBufUsed	db	00h

serialVarsEnd:	equ	$


biosEnd:	equ	$

; Disable the ROM, pop the active IO port from the stack (supplied by monitor),
; then start CP/M
popAndRun:
		LD	A,$01 
		OUT	($38),A

		POP	AF
		CP	$01
		JR	Z,consoleAtB
		LD	A,$01 ;(List is TTY:, Punch is TTY:, Reader is TTY:, Console is CRT:)
		JR	setIOByte
consoleAtB:	LD	A,$00 ;(List is TTY:, Punch is TTY:, Reader is TTY:, Console is TTY:)
setIOByte:	LD	(iobyte),A
		LD	(iobyteo),A	; that's the place where IOByte will be restored from
		JP	bios

;	IM 2 lookup for serial interrupt

		org	0FFE0H
		dw	serialInt


;=================================================================================
; Relocate TPA area from 4100 to 0100 then start CP/M

		org	0FFE8H
		LD	A,$01
		OUT	($38),A

		LD	HL,04100H
		LD	DE,00100H
		LD	BC,08F00H
		LDIR
		JP	bios

		org 0FFFEH
		dw	popAndRun

		.END
