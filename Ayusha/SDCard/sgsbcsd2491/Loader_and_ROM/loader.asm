	.org	$5000

;
; Should be set from current monitor
;
primaryIO	equ	$408A

	ifdef	DTZMYSTACK
	ld	(sp_backup),sp
	ld	sp,my_stack
	endif

	call	sd_init

	ifdef	DTZMYSTACK
	ld	sp,(sp_backup)
	endif
	ret
	
issdhc: db 0

	ifdef	DTZMYSTACK
sp_backup:
	dw	0
	ds	64
my_stack:
	endif
	include <outbytehex.asm>
	include	<outstring.asm>
	include	<sdlib_boot.asm>
	.END
