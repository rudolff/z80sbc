;
; usage:
;	call	outstringinline
;	db	"Sometext",0
; warning:	spoils	hl

outstringinline:
    pop	hl			; read pc from stack
    call	outstring
    inc	hl
    push	hl
    ret
    
; usage:	hl - asciiz

outstring:
    ld	a,(hl)
    cp	0
    jp	nz,outstringoutchar
    ret
outstringoutchar:
    rst	8
    inc	hl
    jp outstring
    