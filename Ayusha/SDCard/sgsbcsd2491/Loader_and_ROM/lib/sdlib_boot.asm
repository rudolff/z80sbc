; DtZ's (http://XEPb.ru/dtz) SD card interface Loader  for 
; Searle Grant's (http://searle.hostei.com/grant/cpm/index.html) SBC
;
;

; SD/MMC RELATED DEFINES:

SD_PORT        equ     $8

; NOTE:
; ***WRITE**
; BIT 0 - SCK, normally low
; BIT 1 - DATA, normally data or HIGH while reading
; BIT 2 - CS, chip selecte
; ***READ**
; BIT 1 - INVERTED data. If you use positive data, comment XOR 1 in 
; sd_read_byte_read_bit:


; Definitions for MMC/SDC command 
SD_CMD0		equ	$40 ; GO_IDLE_STATE  Resets the multimedia card
SD_CMD1		equ	$41	; SEND_OP_COND (MMC)  Activates the card's initialization process
;SD_ACMD41	equ	(0xC0+41)	; SEND_OP_COND (SDC)
SD_CMD8		equ	$48	; SEND_IF_COND
SD_CMD9		equ	$49	; SEND_CSD  : Ask the selected card to send its card specific data
SD_CMD10	equ	$4a	; SEND_CID : Ask the selected card to send its card identification
;SD_CMD12	equ	$4c	; STOP_TRANSMISSION
SD_CMD13	equ	$4d	; SD_STATUS (SDC) Ask the selected card to send its status register

SD_CMD16	equ	$50	; SET_BLOCKLEN Select a block length (in bytes) for all following block commands (Read:between 1-512 and Write:only 512)
SD_CMD17	equ	$51	; READ_SINGLE_BLOCK
;SD_ACMD23	(0xC0+23)	; SET_WR_BLK_ERASE_COUNT (SDC) 
SD_CMD24	equ	$58	; WRITE_BLOCK Writes a block of the size selected by CMD16, the start address must be alligned on a sector boundry, the block length is always 512 bytes
SD_CMD27	equ	$5b	; Programming of the programmable bits of the CSD
SD_CMD41	equ	$69
SD_CMD55	equ	$77	;
SD_CMD58	equ	$7a	;
SD_CMD59	equ	$7b	;



BLOCKSIZE       equ     512     ; SD/MMC block size (bytes)

; all clear

significant_delay:
;	call	cs_high
	ld	bc,$FFFF
significant_delay_loop:
	dec	bc
	xor	a
	or	b
	or	c    
	jr	nz,significant_delay_loop
;	call	cs_low
	ret

cs_high:
	ld	a,6	; CS = 1, DATA = 1, CLK = 0
	out	(SD_PORT),a
	ret

cs_low:
	ld	a,2	; CS = 0, DATA = 1, CLK = 0
	out	(SD_PORT),a
	ret

;********************************************* READING BYTE *****************************************
sd_read_byte:
	push	bc ; b result, c port addr
	push 	hl ; 
	ld c, SD_PORT ;
	ld hl, 3 * 256 + 2 ; h: CS= 0, data=1, clock=1 , l: CS= 0, data=1, clock=0

	; expanded cycle
	; 7 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 6 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 5 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 4 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 3 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 2 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 1 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	; 0 bit
	out (c),h  ; 12, Should be read at Falling edge, but this is a rising one?
	in	a,(SD_PORT) ; 11
	rra	      ; 4, only	lower bit is significant, move it to carry 
	out (c),l ; 12
	rl b      ; 8, move carry to b

	;; total 47 * 8 = 376 clock cycles

	ld	a,b	; holding result in a
	cpl	; our input is inverted
	
	ifdef	DTZDEBUGREADTOO

	push	af
	call	outstringinline
	db	"RB: ",0
	pop	af
	push	af
	call	outbytehex

	pop	af
	endif	; DTZDEBUGREADTOO

	pop hl
	pop de	
	ret

;*********************** SEND COMMAND, WAIT FOR NON-FF  REPLY **************/
sd_write_cmd_and_get_result:
	call	sd_write_cmd
sd_write_cmd_and_get_result_preloop:
	ld	b,0xFF ; was 16
sd_write_cmd_and_get_result_loop:	; Normally this loop executes at least twice,
	push	bc			; FF for the first time
	call	sd_read_byte
	inc	a				; was FF, became 0 ; was command, became command +1
	jp	nz, sd_write_cmd_and_get_result_done
	pop	bc
	djnz	sd_write_cmd_and_get_result_loop
	dec	a	; returning first non-FF byte in A
	ret		;no way - failed
	
sd_write_cmd_and_get_result_done:
	pop	bc		; remove from stack
	dec	a
	ret
;********************* READ BYTES FOR NON-FF REPLY *********************/
sd_wait_for_non_ff:
	push 	bc
	call	sd_write_cmd_and_get_result_preloop
	pop	bc
	ret


;*********************** SEND COMMAND WITH ARGS, WAIT FOR NON-FF REPLY **************
sd_write_cmd_with_args_and_get_result:
	call	sd_write_cmd_with_args
	jp	sd_write_cmd_and_get_result_preloop


;*************************** WRITING 6-BYTE COMMAND **********************************
;  Sends a command with all the parameters = 00 and checksum = $95. Destroys AF.
; Called from sd_init_:


; same, with checksumm in b:
sd_write_cmd_with_checksumm
	push	bc
	jr	sd_write_cmd_pop_checksumm

sd_write_cmd:
	ld	b,$95
	push	bc
sd_write_cmd_pop_checksumm:

; and this FF is significant for some (older) cards!
	push	af
	ld	a,$FF
	call    sd_write_byte
	pop	af

	ifdef	DTZDEBUG
	push	af
	call	outstringinline
	db	"Writing Command: ",0
	pop	af
	push	af
	call	outbytehex
	call	outstringinline
	db $0D,$0A,0
	pop	af
	endif

	call	sd_write_byte
	xor	a
	call	sd_write_byte
	xor	a
	call	sd_write_byte
	xor	a
	call	sd_write_byte
	xor	a
	call	sd_write_byte
	pop	bc			; pushed there by sd_write_cmd or sd_write_cmd_with_checksumm
	ld	a,b			; $95 is only needed when the CARD INIT is being performed			
	call	sd_write_byte		; then this byte is ignored.

; Generally that's a good idea
;	ld	a,$FF
;	call	sd_write_byte

	ifdef	DTZDEBUG
	call	outstringinline
	db $0D,$0A,0
	endif

	ret


; Wrapper for sd_write_cmd_with_args_pop_checksumm
sd_write_cmd_with_args:
; args	bc,de
; third and forth args Low Looow
; first and second args Hii Hi
	ld	h,$95
	push	hl
	jr	sd_write_cmd_with_args_pop_checksumm

sd_write_cmd_with_args_and_checksumm:
; same as above, H is checksumm
	push	hl

;  Sends a command with parameters = 00 and checksum = $95. Destroys AF.
; Called from sd_init_:
sd_write_cmd_with_args_pop_checksumm:		; args	bc,de
	push	de		; third and forth args Low Looow
	push	bc		

; and this FF is significant!
	push	af
	ld	a,$FF
	call    sd_write_byte
	pop	af

;	push	af
;	call	outstringinline
;	db	"Writing Command with args: ",0
;	pop	af
;	push	af
;	call	outbytehex
;	call	outstringinline
;	db $0D,$0A,0
;	pop	af		; command itself
	call	sd_write_byte
	pop	bc
	ld	a,b		; b
	push	bc
	call	sd_write_byte
	pop	bc
	ld	a,c		; c
	call	sd_write_byte
	pop	de
	ld	a,d		; d
	push	de
	call	sd_write_byte
	pop	de
	ld	a,e		; e
	call	sd_write_byte
	pop	hl		; popping checksumm
	ld	a,h		; Normally $95 form CMD0
	call	sd_write_byte		; but most of time this byte is ignored.

; Generally that's a good idea
;	ld	a,$FF
;	call	sd_write_byte

	ret


; Called by sd_write_command
sd_write_byte:
	push	bc
;	push	af
;	call	outstringinline
;	db	"WB: ",0
;	pop	af

	ifdef	DTZDEBUG
	push	af
	call	outbytehex
	pop	af
	endif

	rlca ; rotate bit 7 -> bit 0
	rlca ; rotate bit 0 -> bit 1	
	ld c,a ; c is a temporary variable
	ld b,2 ; b is mask for data bit
	
	; write bit 7
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 6
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 5
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 4
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 3
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 2
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 1
	ld a,c ; 4
	rlca   ; 4
	ld c,a ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

	; write bit 0
	ld a,c ; 4
	rlca   ; 4
	and	b  ; 4
	out (SD_PORT),a ; 11, Data on data line, Clock low
	inc	a		    ; 4, Clk to high
	out (SD_PORT),a	; 11, 

    dec	a		; 4 clock to low, data still on data bus, but noone cares! 
	out (SD_PORT),a ; 11
	pop	bc	; pushed after sd_write_byte
	ret

mmc_send_ticks_cs_high:
	push	bc
	ld	a,7
	out 	(SD_PORT),a	; cs-high,data-high,clock-up

	ifdef	DTZDEBUG
	ld	a,'^'
	rst	8
	endif

	ld	a,6
	out     (SD_PORT),a	; cs-high, data-high, clock-down

	ifdef	DTZDEBUG
	ld	a,'v'
	rst	8
	endif

	pop	bc
	djnz	mmc_send_ticks_cs_high
	ret

; Reads 60h 200h-bytes sectors (3000h bytes) into (HL)
; for the 64k, max sector number is 20000h
; for the 128k, max sector is 40000hs
;sd_read_60h_sectors:
;	ld	b,0
;	push	bc
;sd_read_60h_sectors_loop
;	ld	a,SD_CMD17
;	call	sd_write_cmd_and_get_result
;	ld	a,$0D
;	rst	8
;	ld	a,$0A
;	rst	8
;	ld	bc,$210
;readblockloop:
;	push	bc
;	call	sd_read_byte
;	pop	bc
;	dec	bc
;	xor	a
;	or	b
;	or	c	; I suppose a will be 0 only if b and c are 0 
;	jp	nz,readblockloop

; ********************************* External Entry Point ***************************************

sd_init:      
	call	outstringinline
	db	"Initing card",$0D,$0A,0
        call   cs_high                 ; set cs high
	ld	b,80
	call	mmc_send_ticks_cs_high
	call	cs_low
;	ld	a,MMC_GO_IDLE_STATE
	ld	a,SD_CMD0

	call	sd_write_cmd

	ifdef	DTZDEBUG
	call	outstringinline
	db	"SD_CMD0 Command written",$0A,$0D,0
	endif

;	ld	b,$FF		; wait for 01 response for 16 byte ticks - should be more then enought
	ld	bc,$FFFF

sd_init_wait_init:
	push bc
        call    sd_read_byte
        dec     a
        jp      z,sd_init_spi_done      ; got 01 response, cool!
;        cp      $FE                     ; compare with FF
;        jp      nc,sd_init_wait_init_cont ; got smth - not FF and not FE - reiniting again - do not forget to loop this jump
;        ifdef DTZDEBUG
;        push    af
;	call	outstringinline
;        db      "SD_CMD0 returned ",0
;        pop     af
;        call    outbytehex
;        call    outstringinline
;        db      " instead of 1",$0D,$0A,0
;        endif
;
;        jr      sd_init
;
;sd_init_wait_init_cont:
;        djnz    sd_init_wait_init
	pop bc
	dec	bc
	ld	a,b
	or	c
	jr	nz,sd_init_wait_init

        call    outstringinline
        db      "Unable to SD_CMD0",$0A,$0D,0
        jp      sd_init_exit

sd_init_spi_done:
	pop	bc	; pushed there in loop sd_init_wait_init

	ifdef	DTZDEBUG
	call	outstringinline
	db	"**Entered SPI mode**",$0A,$0D,"Lets reset it",$0A,$0D,0 ; or at least I think we entered
	endif

	ifndef	DTZNOSDHC

; CMD8 are send in idle state, i.e after CMD0 , but before CMD1/ACMD41
; so normally it should return 1, ie. it stays in idle state
;
; SEND_IF_COND, should return R7
sd_init_cmd8:
	ld	a,SD_CMD8
	ld	h,$87 ; pre-calculated value
	ld	de,$1AA ; 1 is 2.7-3.6V, AA is thing to echo.
	ld	bc,0
	call	sd_write_cmd_with_args_and_checksumm
	call	sd_wait_for_non_ff

	ifdef	DTZDEBUG
	push	af
	call	outstringinline
	db	"Read from CMD8 ",0
	pop	af
	push	af
	call	outbytehex	; if 5 in A - means CMD8 fails
				; if bit 0 is 0 - not a SDHC (EyeFi behaves so!)		
	pop	af
	endif

	cp	5
	jr	nz,sd_init_cmd8_not_error
	call	outstringinline
	db	"Card is V1 or MMC even",$0D,$0A,0
	jp	sd_init_cmd1	; Go to sd_init_cmd1 and do not try ACMD41 even
	ld	b,a
	and	$FE		; 1 and 0 replies are ok; 1 is expectable
	jr	z,sd_init_cmd8_not_error
	ld	a,b
	call	outbytehex
	call	outstringinline
	db	" - CMD8 returned;Card is neither V1,V2,MMC!",$0D,$0A,0
	jp	sd_init_cmd1	; Go to sd_init_cmd1 and do not try ACMD41 even

sd_init_cmd8_not_error:
;	and	1			; wrong way of doing it
;	jr	z,sd_init_cmd8_suspect_not_sdhc
;	call	outstringinline
;	db	"Looks like SDHC",$0D,$0A,0
;	jr	sd_init_cmd8_told_about_sdhc
;sd_init_cmd8_suspect_not_sdhc:
;	call	outstringinline
;	db	"Looks like not SDHC",$0D,$0A,0
;sd_init_cmd8_told_about_sdhc:

	ld	b,4		; 5 bytes should be read; one was read allready
sd_init_cmd8_loop:
	push	bc
	push	de	; here in de we will collect two last bytes
	call	sd_read_byte
	pop	de
	ld	d,e	; rotating <DE<A
	ld	e,a

	ifdef	DTZDEBUG
	push	de
	call	outbytehex
	pop	de
	endif

	pop	bc
	djnz	sd_init_cmd8_loop
	ld	a,1			; checking for 1AA signature 
	cp	d
	jp	nz,sd_init_cmd8_not_1aa
	ld	a,$AA
	cp	e
	jp	nz,sd_init_cmd8_not_1aa
	call	outstringinline
	db	"Card is V2",$0D,$0A,0

;	call	outstringinline
;	db	"Sending CRC OFF/ON",$0D,$0A,0
;	ld	a,SD_CMD59
;	call	sd_write_cmd_and_get_result
;	call	outbytehex
;	call	outstringinline
;	db	"<- that was the result of CMD59",$0D,$0A,0

; http://elm-chan.org/docs/mmc/mmc_e.html
; "And then initiate initialization with ACMD41 with HCS flag (bit 30)"


	ld	b,5
sd_init_acmd41_after_cmd8:
	push	bc
	ld	a,SD_CMD55
	call	sd_write_cmd_and_get_result
	ifdef	DTZDEBUG
	call	outbytehex
	call	outstringinline
	db	"<- CMD55:",$0D,$0A,0
	endif
	ld	a,SD_CMD41
	ld	bc,$4000 ; bit 30 set, right?
	ld	de,0
	call	sd_write_cmd_with_args_and_get_result
	jp	z,sd_init_acmd41_done	; if we got 0 - means we left Idle
;state, so inited the card
;	call	outbytehex
;	call	outstringinline
;	db	"<- that was the result of ACMD41 with HCS flag",$0D,$0A,0
	call	significant_delay
	pop	bc
	djnz	sd_init_acmd41_after_cmd8

	jr	sd_init_cmd8_done
sd_init_cmd8_not_1aa:
	call	outstringinline
	db	"Not a V2 card",$0D,$0A,0

sd_init_cmd8_done:
	endif	
;		//DTZNOSDHC


sd_init_cmd1:
	ld	b,$FF		; if we got here - means ACMD41 failed and we will try with CMD1
	ifdef	DTZDEBUG
	ld	b,$5
	endif
;	ld	bc,$500
sd_init_init_loop:
	push	bc
	ld	a,SD_CMD1
	call	sd_write_cmd_and_get_result
	jr	nz,sd_init_init_not_done_yet
	jr	sd_init_cmd1_done
sd_init_init_not_done_yet:
	ifdef	DTZDEBUG
	push	af
        call    outstringinline
        db      "SD_CMD1 returned ",0
        pop     af
        call    outbytehex
        call    outstringinline
        db      " instead of 0",$0D,$0A,0
	endif

	call	significant_delay
	pop	bc
	djnz	sd_init_init_loop
	jp	sd_not_cmd1


sd_init_cmd1_done:
	ifdef	DTZDEBUG
	call	outstringinline
	db	"CMD1 ok",$0D,$0A,0
	endif
	jr	sd_init_init_done

sd_init_acmd41_done:
	ifdef	DTZDEBUG
	call	outstringinline
	db	"ACMD41 ok",$0D,$0A,0
	endif

sd_init_init_done:
; here we get in any case if card was inited from idle mode 

; pushed after sd_init_acmd41_after_cmd8:
; or after sd_init_init_loop:
	pop	bc


; **************** CMD10 *********************
; Prining some card info - we do not care, but it is interesting
;
;

	ld      a,SD_CMD10
	call	sd_write_cmd_with_args_and_get_result
	or	a
	jp	nz,sd_init_cant_get_cmd10_warning

	ld	b,16
sd_init_get_cmd10_wait_FE:		; waiting for data token
	call	sd_read_byte
	cp	$FE
	jp	z,sd_init_get_cmd10_read_data
	djnz	sd_init_get_cmd10_wait_FE
	jp	sd_init_cant_get_cmd10_fe_warning

sd_init_get_cmd10_read_data:

	ifdef	DTZDEBUG
	call	outstringinline
	db	"*Got CMD10 data token*",$0D,$0A,0
	endif

	ld	b,3	;$16 bytes + 2 crc ; first 3 bytes is manufacturer ID + etc ID
sd_init_get_cmd10_loop_0:
	call	sd_read_byte
	djnz	sd_init_get_cmd10_loop_0
	call	outstringinline
	db	"Card ID:",0
	ld	b,5	;next 5 bytes - ascii name of catd
sd_init_get_cmd10_loop_1: ;	printing ASCII card id
	call	sd_read_byte
	rst	8
	djnz	sd_init_get_cmd10_loop_1
	ld	a,$0D
	rst	8
	ld	a,$0A
	rst	8
	ld	b,10	; read to the end of data
sd_init_get_cmd10_loop_2:
	call	sd_read_byte
	djnz	sd_init_get_cmd10_loop_2
	jp	init_mmc_go_cmd10_done

sd_init_cant_get_cmd10_warning: ; just a warning, but very bad
	push	af
	call	outstringinline
	db	"Warning: cannot get info via CMD10: ",0
	pop	af
	call	outbytehex
	call	outstringinline
	db	" instead of 0",$0D,$0A,0
	jp	init_mmc_go_cmd10_done

sd_init_cant_get_cmd10_fe_warning: ; just a warning, but very bad
	call	outstringinline
	db	"Warning: cant get data token for CMD10",$0D,$0A,0
init_mmc_go_cmd10_done:
; ******** End of CMD10 ********


; "After the initialization completed, read OCR register with CMD58 and check CCS flag (bit 30). 
; When it is set, the card is a high capacity card that known as SDHC/SDXC. "

;	Let's read CMD58/GetCCS - some voltage stuff - here the OCR register 
; 	And the way to find out if the card is SDHC
	ifdef	DTZDEBUG
	call	outstringinline
	db	"CMD58:",$0D,$0A,0
	endif
	ld	a,SD_CMD58		; 7A
	call	sd_write_cmd_and_get_result ; and lets get 4 bytes of data
	and	a
	jp	nz,cmd58_failed
	ifdef	DTZDEBUG
	call	outbytehex
	endif
	call	sd_read_byte	; First byte of OCR register
	ifdef	DTZDEBUG
	push	af
	call	outbytehex
	pop	af
	endif
	and	$40	; that should be 40 for SD
	jr	z,cmd58_notsdhc
	call	outstringinline
	db	"SDHC card",$0D,$0A,0
	ld	a,1
	jr	cmd_58_cont
cmd58_failed:
	call	outbytehex
	call	outstringinline
	db	"CMD58 failed, assuming "
cmd58_notsdhc:
	call	outstringinline
	db	"SD card",$0D,$0A,0
	xor	a
cmd_58_cont:
	ifndef	DTZNOSDHC
	ld	(issdhc),a
	endif
	call	sd_read_byte	; second byte of OCR register
	ifdef	DTZDEBUG
	call	outbytehex
	endif
	call	sd_read_byte	; third byte of OCR register
	ifdef	DTZDEBUG
	call	outbytehex
	endif
	call	sd_read_byte	; fourth byte of OCR register 
	ifdef	DTZDEBUG	
	call	outbytehex
	call	outstringinline
	db	"Done with CMD58",$0D,$0A,0
	endif

;	Setting sector size
        ld      bc,0            ; high 2 bytes
        ld      de,BLOCKSIZE        ; Means 512, or I bet you are in trouble
        ld      a,SD_CMD16      ; Set sector size to 512 bytes
        call    sd_write_cmd_with_args_and_get_result
        or      a
        jp      nz,sd_not_sector ; This command may fail on some old cards, which default sector size is 512, however
    ; No, that was a speach of SD HC cards. Or not? Do not care, this command works ;-)



; Prining some card info - we do not care, but it is interesting
; Card size somehow is fetched by this code
;init_mmc_go_cmd9:
;	ld      a,SD_CMD9
;	call	sd_write_cmd_with_args_and_get_result
;	ld	b,20	; Seems to have 20 significant bytes
;sd_init_get_cmd9_loop_0:
;	call	sd_read_byte
;	djnz	sd_init_get_cmd9_loop_0


; SD:
; Now we have to read $3000 bytes = $200 bytes sectors.
; That means we have to read 
; b  c  d  e for sd_write_cmd_with_args_and_get_result
;00 00 00 00 for the sector 0
;00 00 02 00 for the sector 1
;00 00 3E 00 for last sector
;
; SDHC:
; Now we have to read $3000 bytes = $200 bytes sectors.
; That means we have to read 
; b  c  d  e for sd_write_cmd_with_args_and_get_result
;00 00 00 00 for the sector 0
;00 00 00 02 for the sector 1
;00 00 00 3E for last sector

load_cpm:
	ld	d,$FE	; d == -2 ; will be 0 for the first iteration
	ld	hl,$C000	; CP/M Load address
load_cpm_loop:	

readblock:
	ld	bc,0
	ld	e,0
	inc	d
	inc	d
;	ld	a,$4		; For now, lets's try to load only two sectors
	ld	a,$40		; loop from 0 to $3e
	cp	d
	jp	z,load_cpm_done


	push	de		; Push sector counter
	push	hl		; Push memory pointer

	ifndef	DTZNOSDHC
; For SDHC , we have to exchange E and D
	ld	a,(issdhc)
	and	a
	jr	z,readblock_not_sdhc
	ld	a,d
	ld	d,e
	srl	a	; 2 -> 1, 3E -> 1F
	ld	e,a

readblock_not_sdhc:
	endif

	ld	a,SD_CMD17
	call	sd_write_cmd_with_args_and_get_result

	ifdef	DTZDEBUG
	push	af
	ld	a,$0D
	rst	8
	ld	a,$0A
	rst	8
	pop	af
	call	outbytehex
	ld	a,$0D
	rst	8
	ld	a,$0A
	rst	8
	endif

		; Should wait for data token started with FE
		; and skip some bytes

	call	sd_wait_for_non_ff
	cp	$FE
	jp	nz, load_cpm_cannot_read_block
;	call	outstringinline
;	db	"Got FE, reading block",$0D,$0A,0
	ld	bc,$200 ; normally $200 , and then read a CRC

;	call	outstringinline
;	db	"HL:",0
	pop	hl		; pop memptr
;	push	hl
;	ld	a,h
;	call	outbytehex
;	ld	a,l
;	call	outbytehex
;	call	outstringinline
;	db	$0D,$0A,0
;	pop	hl		; pop mempt
readblockloop:
	push	bc		; Push byte-inside-sector counter
	push	hl
	call	sd_read_byte
;	push	af
;	call	outbytehex
;	pop	af
	pop	hl
	ld	(hl),a		; Move loaded byte to memory
	inc	hl		; memory counter++
	pop	bc		; Pop byte-inside-sector counter
	dec	bc
	xor	a
	or	b
	or	c	; I suppose A will be 0 only if B and C are 0 
	jp	nz,readblockloop
	push	hl		; push memory pointer to stack	

	ld	a,'.'
	rst	8
;	ld	a,$0D
;	rst	8
;	ld	a,$0A
;	rst	8
	call	sd_read_byte	; Checksum
	call	sd_read_byte	; Checking checksum is generally a good idea

	pop	hl	; pop memory counter
	pop	de	; Pop sector counter
	jp	load_cpm_loop
	
load_cpm_done:
	call	outstringinline
	db	$0D,$0A,"OS Loaded",$0D,$0A,0


;	ld	hl,$D000
;dump:
;	ld	a,h
;	call	outbytehex
;	ld	a,l
;	call	outbytehex
;	ld	a,':'
;	rst	8
;	ld	b,10h
;	push	bc
;dump_loop1:
;	ld	a,(hl)
;	inc	hl
;	call	outbytehex
;	djnz	dump_loop1

;	pop	bc
;        djnz	dump


; Actually run CP/M

;	ld	a,(issdhc)
;	push	af

	ld	a,(primaryIO)	; PrimaryIO from monitor
	push	af	; should be pushed to stack

	ifdef	DTZDEBUG
	call	outbytehex
	ld	a,($fffe)
	call	outbytehex
	ld	a,($ffff)
	call	outbytehex
	call	outstringinline
	db	"Go Garry go",$0A,$0D,0
	ld	hl,0
	add	hl,sp
	ld	a,l
	call	outbytehex
	endif
	ld	hl,($fffe)
	jp	(hl)

; Global exit point
sd_init_exit:
	ret

; Show specific error messages

load_cpm_cannot_read_block:
	pop	hl	; remove pointer from stack
	pop	hl	; remove sector counter from stack

	push	af
	call	outstringinline
	db	"Cannot read block: error ",0
	pop	af
	call	outbytehex
	call	outstringinline
	db	$0D,$0A,0
	jp	sd_init_exit

;sd_no_cmd8:
;	call	outstringinline
;	db	"Cannot send CMD8 - voltage problem?",$0D,$0A,0
;	jp	sd_init_exit

sd_not_cmd1:
	call	outstringinline
	db	"Cannot send CMD1",$0D,$0A,0
	jp	sd_init_exit

sd_not_sector:
	call	outstringinline
	db	"Unable to set 512b sector",$0D,$0A,0
	jp	sd_init_exit

;sd_not_cmd41:
;	call	outstringinline
;	db	"CMD41 failed",$0D,$0A,0
;	jp	sd_init_exit

;sd_not_second55:
;	call	outstringinline
;	db	"Second 55 command failed",$0D,$0A,0
;	jp	sd_init_exit

;sd_not_sd_card:
;	call	outstringinline
;	db	"SD card not ready (CMD41)",$0D,$0A,0
;	jp	sd_init_exit



;sd_toggle_cs:
;	call	cs_high
;	ld	b,255
;sd_toggle_cs_loop:
;	djnz	sd_toggle_cs_loop
;	call	cs_low
;	ret
