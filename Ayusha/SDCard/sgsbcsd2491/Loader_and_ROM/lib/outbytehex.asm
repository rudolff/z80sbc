outwordhex:
    push	af
    push	hl
    ld	a,h
    call	outbytehexplain
    pop	hl
    ld	a,l
    call	outbytehexplain
    pop	af
    ret

outbytehex:
    push	af
    ld	a,'['
    rst	8
    pop	af
    push	af
    call	outbytehexplain
    ld	a,']'
    rst	8
    ld	a,'['
    rst	8
    pop	af
    cp	$20
    jp	c,outbytehexcharstar
;    cp	$7f
;    jp	c,outbytehexchar
    jp	outbytehexchar
outbytehexcharstar:
    ld	a,'*'
outbytehexchar:
    rst	8
    ld	a,']'
    rst	8
    ret

outbytehexplain:
  push	af
  and	$f0
  rra
  rra
  rra
  rra
  call	outnibble
  pop	af
  and	$0f
  call	outnibble
  ret

outnibble:
  and	$0f
  cp	$0a
  jp	nc,outnibbleless
  add	a,48
  rst	8
  ret
outnibbleless:
  add	a,55	;	A - 10
  rst	8
  ret