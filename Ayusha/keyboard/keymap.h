#ifndef KEYMAP_H
#define KEYMAP_H

extern const unsigned char keymap[];
extern const unsigned char keymap_size;
extern const unsigned char keymap_shift[];
extern const unsigned char keymap_shift_size;
extern const unsigned char keymap_caps[];
extern const unsigned char keymap_caps_size;
extern const unsigned char keymap_caps_shift[];
extern const unsigned char keymap_caps_shift_size;
extern const unsigned char keymap_rus[];
extern const unsigned char keymap_rus_size;
extern const unsigned char keymap_rus_shift[];
extern const unsigned char keymap_rus_shift_size;
extern const unsigned char keymap_rus_caps[];
extern const unsigned char keymap_rus_caps_size;
extern const unsigned char keymap_rus_caps_shift[];
extern const unsigned char keymap_rus_caps_shift_size;
extern const unsigned char keymap_ctrl[];
extern const unsigned char keymap_ctrl_size;

#endif
