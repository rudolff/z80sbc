// ps/2 scan codes


#ifndef SCANCODES_H
#define SCANCODES_H

#define SC_A	0x1c
#define SC_B	0x32
#define SC_C	0x21
#define SC_D	0x23
#define SC_E	0x24
#define SC_F	0x2b
#define SC_G	0x34
#define SC_H	0x33
#define SC_I	0x43
#define SC_J	0x3b
#define SC_K	0x42
#define SC_L	0x4b
#define SC_M	0x3a
#define SC_N	0x31
#define SC_O	0x44
#define SC_P	0x4d
#define SC_Q	0x15
#define SC_R	0x2d
#define SC_S	0x1b
#define SC_T	0x2c
#define SC_U	0x3c
#define SC_V	0x2a
#define SC_W	0x1d
#define SC_X	0x22
#define SC_Y	0x35
#define SC_Z	0x1a

#define SC_0	0x45
#define SC_1	0x16
#define SC_2	0x1e
#define SC_3	0x26
#define SC_4	0x25
#define SC_5	0x2e
#define SC_6	0x36
#define SC_7	0x3d
#define SC_8	0x3e
#define SC_9	0x46

#define SC_SPACE		0x29
#define SC_BKSPACE		0x66
#define SC_COMMA		0x41
#define SC_PERIOD		0x49
#define SC_SLASH		0x4a
#define SC_SEMICOLON		0x4c
#define SC_DASH			0x4e
#define SC_APOSTROPHE		0x52
#define SC_EQUALS		0x55
#define SC_RETURN		0x5a

#define SC_ESC			0x76
#define SC_R_4			0x6b
#define SC_R_6			0x74
#define SC_R_8			0x75
#define SC_R_2			0x72
#define SC_R_0			0x70
#define SC_R_1			0x69
#define SC_R_3			0x7a
#define SC_R_5			0x73
#define SC_R_7			0x6c
#define SC_R_9			0x7d
#define SC_R_DOT			0x71
#define SC_R_STAR		0x7c
#define SC_R_MINUS		0x7b
#define SC_R_PLUS			0x79

#define SC_LSHIFT		0x12
#define SC_RSHIFT		0x59
#define SC_CTRL			0x14
#define SC_ALT			0x11

#define SC_SBRACKET_OP		0x54
#define SC_SBRACKET_CL		0x5b
#define SC_BACKSLASH		0x5d
#define SC_REVQUOTE		0x0e

#define SC_CAPSLK		0x58
#define SC_SCRLLK		0x7e
#define SC_TAB			0x0d
#define SC_TILDA		0x0e

#endif
