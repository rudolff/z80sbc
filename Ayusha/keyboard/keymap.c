/*
Keymap PS/2 to ASCII
Locale: en_US + CP866

the ps/2 e0-prefix for special keys (cursors etc) is
discarded in the reading-routine
*/

#include "scancodes.h"

const unsigned char keymap[] = {
	SC_A, 'a',
	SC_B, 'b',
	SC_C, 'c',
	SC_D, 'd',
	SC_E, 'e',
	SC_F, 'f',
	SC_G, 'g',
	SC_H, 'h',
	SC_I, 'i',
	SC_J, 'j',
	SC_K, 'k',
	SC_L, 'l',
	SC_M, 'm',
	SC_N, 'n',
	SC_O, 'o',
	SC_P, 'p',
	SC_Q, 'q',
	SC_R, 'r',
	SC_S, 's',
	SC_T, 't',
	SC_U, 'u',
	SC_V, 'v',
	SC_W, 'w',
	SC_X, 'x',
	SC_Y, 'y',
	SC_Z, 'z',

	SC_0, '0',
	SC_1, '1',
	SC_2, '2',
	SC_3, '3',
	SC_4, '4',
	SC_5, '5',
	SC_6, '6',
	SC_7, '7',
	SC_8, '8',
	SC_9, '9',

	SC_SPACE,		' ',
	SC_BKSPACE,		0x08,
	SC_COMMA,		',',
	SC_PERIOD,		'.',
	SC_SLASH,		'/',
	SC_SEMICOLON,		';',
	SC_DASH,		'-',
	SC_APOSTROPHE,		'\'',
	SC_EQUALS,		'=',
	SC_RETURN,		0x0d,
	SC_TAB,			0x09,
	SC_ESC,				0x1b,
	SC_SBRACKET_OP,		'[',
	SC_SBRACKET_CL,		']',
	SC_BACKSLASH,		'\\',
	SC_TILDA,		'`',

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_size = sizeof( keymap ) / 2;

const unsigned char keymap_shift[] = { 
	SC_A, 'A',
	SC_B, 'B',
	SC_C, 'C',
	SC_D, 'D',
	SC_E, 'E',
	SC_F, 'F',
	SC_G, 'G',
	SC_H, 'H',
	SC_I, 'I',
	SC_J, 'J',
	SC_K, 'K',
	SC_L, 'L',
	SC_M, 'M',
	SC_N, 'N',
	SC_O, 'O',
	SC_P, 'P',
	SC_Q, 'Q',
	SC_R, 'R',
	SC_S, 'S',
	SC_T, 'T',
	SC_U, 'U',
	SC_V, 'V',
	SC_W, 'W',
	SC_X, 'X',
	SC_Y, 'Y',
	SC_Z, 'Z',

	SC_1, '!',
	SC_2, '@',
	SC_3, '#',
	SC_4, '$',
	SC_5, '%',
	SC_6, '^',	
	SC_7, '&',
	SC_8, '*',
	SC_9, '(',
	SC_0, ')',

	SC_COMMA,		'<',
	SC_PERIOD,		'>',
	SC_SLASH,		'?',
	SC_SEMICOLON,		':',
	SC_APOSTROPHE,		'"',
	SC_EQUALS,		'+',
	SC_SBRACKET_OP,		'{',
	SC_SBRACKET_CL,		'}',
	SC_DASH,		'_',
	SC_BACKSLASH,		'|',
	SC_TILDA,		'~',

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_shift_size = sizeof( keymap_shift ) / 2;

const unsigned char keymap_caps[] = {
	SC_A, 'A',
	SC_B, 'B',
	SC_C, 'C',
	SC_D, 'D',
	SC_E, 'E',
	SC_F, 'F',
	SC_G, 'G',
	SC_H, 'H',
	SC_I, 'I',
	SC_J, 'J',
	SC_K, 'K',
	SC_L, 'L',
	SC_M, 'M',
	SC_N, 'N',
	SC_O, 'O',
	SC_P, 'P',
	SC_Q, 'Q',
	SC_R, 'R',
	SC_S, 'S',
	SC_T, 'T',
	SC_U, 'U',
	SC_V, 'V',
	SC_W, 'W',
	SC_X, 'X',
	SC_Y, 'Y',
	SC_Z, 'Z',

	SC_0, '0',
	SC_1, '1',
	SC_2, '2',
	SC_3, '3',
	SC_4, '4',
	SC_5, '5',
	SC_6, '6',
	SC_7, '7',
	SC_8, '8',
	SC_9, '9',

	SC_SPACE,		' ',
	SC_BKSPACE,		0x08,
	SC_COMMA,		',',
	SC_PERIOD,		'.',
	SC_SLASH,		'/',
	SC_SEMICOLON,		';',
	SC_DASH,		'-',
	SC_APOSTROPHE,		'\'',
	SC_EQUALS,		'=',
	SC_RETURN,		0x0d,
	SC_TAB,			0x09,
	SC_ESC,				0x1b,
	SC_SBRACKET_OP,		'[',
	SC_SBRACKET_CL,		']',
	SC_BACKSLASH,		'\\',
	SC_TILDA,		'`',

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_caps_size = sizeof( keymap_caps ) / 2;

const unsigned char keymap_caps_shift[] = {
	SC_A, 'a',
	SC_B, 'b',
	SC_C, 'c',
	SC_D, 'd',
	SC_E, 'e',
	SC_F, 'f',
	SC_G, 'g',
	SC_H, 'h',
	SC_I, 'i',
	SC_J, 'j',
	SC_K, 'k',
	SC_L, 'l',
	SC_M, 'm',
	SC_N, 'n',
	SC_O, 'o',
	SC_P, 'p',
	SC_Q, 'q',
	SC_R, 'r',
	SC_S, 's',
	SC_T, 't',
	SC_U, 'u',
	SC_V, 'v',
	SC_W, 'w',
	SC_X, 'x',
	SC_Y, 'y',
	SC_Z, 'z',

	SC_1, '!',
	SC_2, '@',
	SC_3, '#',
	SC_4, '$',
	SC_5, '%',
	SC_6, '^',	
	SC_7, '&',
	SC_8, '*',
	SC_9, '(',
	SC_0, ')',

	SC_COMMA,		'<',
	SC_PERIOD,		'>',
	SC_SLASH,		'?',
	SC_SEMICOLON,		':',
	SC_APOSTROPHE,		'"',
	SC_EQUALS,		'+',
	SC_SBRACKET_OP,		'{',
	SC_SBRACKET_CL,		'}',
	SC_DASH,		'_',
	SC_BACKSLASH,		'|',
	SC_TILDA,		'~',

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_caps_shift_size = sizeof( keymap_caps_shift ) / 2;

const unsigned char keymap_rus[] = {
	SC_A, '�',
	SC_B, '�',
	SC_C, '�',
	SC_D, '�',
	SC_E, '�',
	SC_F, '�',
	SC_G, '�',
	SC_H, '�',
	SC_I, '�',
	SC_J, '�',
	SC_K, '�',
	SC_L, '�',
	SC_M, '�',
	SC_N, '�',
	SC_O, '�',
	SC_P, '�',
	SC_Q, '�',
	SC_R, '�',
	SC_S, '�',
	SC_T, '�',
	SC_U, '�',
	SC_V, '�',
	SC_W, '�',
	SC_X, '�',
	SC_Y, '�',
	SC_Z, '�',

	SC_0, '0',
	SC_1, '1',
	SC_2, '2',
	SC_3, '3',
	SC_4, '4',
	SC_5, '5',
	SC_6, '6',
	SC_7, '7',
	SC_8, '8',
	SC_9, '9',

	SC_SPACE,		' ',
	SC_BKSPACE,		0x08,
	SC_COMMA,		'�',
	SC_PERIOD,		'�',
	SC_SLASH,		'.',
	SC_SEMICOLON,	'�',
	SC_DASH,		'-',
	SC_APOSTROPHE,	'�',
	SC_EQUALS,		'=',
	SC_RETURN,		0x0d,
	SC_TAB,			0x09,
	SC_ESC,				0x1b,
	SC_SBRACKET_OP,		'�',
	SC_SBRACKET_CL,		'�',
	SC_BACKSLASH,		'/',
	SC_TILDA,		0xf1,

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',

};
const unsigned char keymap_rus_size = sizeof( keymap_rus ) / 2;

const unsigned char keymap_rus_shift[] = {
	SC_A, '�',
	SC_B, '�',
	SC_C, '�',
	SC_D, '�',
	SC_E, '�',
	SC_F, '�',
	SC_G, '�',
	SC_H, '�',
	SC_I, '�',
	SC_J, '�',
	SC_K, '�',
	SC_L, '�',
	SC_M, '�',
	SC_N, '�',
	SC_O, '�',
	SC_P, '�',
	SC_Q, '�',
	SC_R, '�',
	SC_S, '�',
	SC_T, '�',
	SC_U, '�',
	SC_V, '�',
	SC_W, '�',
	SC_X, '�',
	SC_Y, '�',
	SC_Z, '�',

	SC_1, '!',
	SC_2, '"',
	SC_3, '�',
	SC_4, ';',
	SC_5, '%',
	SC_6, ':',	
	SC_7, '?',
	SC_8, '*',
	SC_9, '(',
	SC_0, ')',

	SC_COMMA,		'�',
	SC_PERIOD,		'�',
	SC_SLASH,		',',
	SC_SEMICOLON,	'�',
	SC_APOSTROPHE,	'�',
	SC_EQUALS,		'+',
	SC_DASH,			'_',
	SC_BACKSLASH,		'/',
	SC_TILDA,		0xf0,
	SC_SBRACKET_OP,		0x95,
	SC_SBRACKET_CL,		0x9a,

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};

const unsigned char keymap_rus_shift_size = sizeof( keymap_rus_shift ) / 2;

const unsigned char keymap_rus_caps[] = {
	SC_A, '�',
	SC_B, '�',
	SC_C, '�',
	SC_D, '�',
	SC_E, '�',
	SC_F, '�',
	SC_G, '�',
	SC_H, '�',
	SC_I, '�',
	SC_J, '�',
	SC_K, '�',
	SC_L, '�',
	SC_M, '�',
	SC_N, '�',
	SC_O, '�',
	SC_P, '�',
	SC_Q, '�',
	SC_R, '�',
	SC_S, '�',
	SC_T, '�',
	SC_U, '�',
	SC_V, '�',
	SC_W, '�',
	SC_X, '�',
	SC_Y, '�',
	SC_Z, '�',

	SC_0, '0',
	SC_1, '1',
	SC_2, '2',
	SC_3, '3',
	SC_4, '4',
	SC_5, '5',
	SC_6, '6',
	SC_7, '7',
	SC_8, '8',
	SC_9, '9',

	SC_SPACE,		' ',
	SC_BKSPACE,		0x08,
	SC_COMMA,		'�',
	SC_PERIOD,		'�',
	SC_SLASH,		'.',
	SC_SEMICOLON,	'�',
	SC_DASH,		'-',
	SC_APOSTROPHE,	'�',
	SC_EQUALS,		'=',
	SC_RETURN,		0x0d,
	SC_TAB,			0x09,

	SC_ESC,				0x1b,
	SC_SBRACKET_OP,		0x95,
	SC_SBRACKET_CL,		0x9a,
	SC_BACKSLASH,		'/',
	SC_TILDA,		0xf0,

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_rus_caps_size = sizeof( keymap_rus_caps ) / 2;

const unsigned char keymap_rus_caps_shift[] = {
	SC_A, '�',
	SC_B, '�',
	SC_C, '�',
	SC_D, '�',
	SC_E, '�',
	SC_F, '�',
	SC_G, '�',
	SC_H, '�',
	SC_I, '�',
	SC_J, '�',
	SC_K, '�',
	SC_L, '�',
	SC_M, '�',
	SC_N, '�',
	SC_O, '�',
	SC_P, '�',
	SC_Q, '�',
	SC_R, '�',
	SC_S, '�',
	SC_T, '�',
	SC_U, '�',
	SC_V, '�',
	SC_W, '�',
	SC_X, '�',
	SC_Y, '�',
	SC_Z, '�',

	SC_1, '!',
	SC_2, '"',
	SC_3, '�',
	SC_4, ';',
	SC_5, '%',
	SC_6, ':',	
	SC_7, '?',
	SC_8, '*',
	SC_9, '(',
	SC_0, ')',

	SC_SPACE,		' ',
	SC_BKSPACE,		0x08,
	SC_COMMA,		'�',
	SC_PERIOD,		'�',
	SC_SLASH,		',',
	SC_SEMICOLON,	'�',
	SC_DASH,		'_',
	SC_APOSTROPHE,	'�',
	SC_EQUALS,		'+',
	SC_RETURN,		0x0d,
	SC_TAB,			0x09,
	SC_ESC,				0x1b,
	SC_SBRACKET_OP,		'�',
	SC_SBRACKET_CL,		'�',
	SC_BACKSLASH,		'/',
	SC_TILDA,		0xf1,

	 SC_R_4,			'4',
	 SC_R_6,			'6',
	 SC_R_8,			'8',
	 SC_R_2,			'2',
	 SC_R_0,			'0',
	 SC_R_1,			'1',
	 SC_R_3,			'3',
	 SC_R_5,			'5',
	 SC_R_7,			'7',
	 SC_R_9,			'9',
	 SC_R_DOT,		'.',
	 SC_R_STAR,		'*',
	 SC_R_MINUS,		'-',
	 SC_R_PLUS,		'+',
};
const unsigned char keymap_rus_caps_shift_size = sizeof( keymap_rus_caps_shift ) / 2;

const unsigned char keymap_ctrl[] = {
	SC_A, 0x01,
	SC_B, 0x02,
	SC_C, 0x03,
	SC_D, 0x04,
	SC_E, 0x05,
	SC_F, 0x06,
	SC_G, 0x07,
	SC_H, 0x08,
	SC_I, 0x09,
	SC_J, 0x0a,
	SC_K, 0x0b,
	SC_L, 0x0c,
	SC_M, 0x0d,
	SC_N, 0x0e,
	SC_O, 0x0f,
	SC_P, 0x10,
	SC_Q, 0x11,
	SC_R, 0x12,
	SC_S, 0x13,
	SC_T, 0x14,
	SC_U, 0x15,
	SC_V, 0x16,
	SC_W, 0x17,
	SC_X, 0x18,
	SC_Y, 0x19,
	SC_Z, 0x1a,

	SC_DASH,		0x1f,
	SC_BACKSLASH,		0x1c,
	SC_EQUALS,		0x1e,
	SC_TILDA,		0x1d,
};
const unsigned char keymap_ctrl_size = sizeof( keymap_ctrl ) / 2;

