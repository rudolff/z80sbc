/* ---------------------------------------------------------
 * PS/2 to ASCII-Keyboard Converter
 * Copyright (c) 2008 Sebastian Kienzl <seb at riot dot org>
 * CP866 from RW6HRM (2018)
 * for PIC 16F684
 * Compile with HT-PICC
 * designed to run at 4MHz (internal osc)
 */

#include <pic.h>
#include "delay.h"
#include "keymap.h"
#include "scancodes.h"

__CONFIG(INTIO&WDTDIS&PWRTEN&MCLRDIS&UNPROTECT&UNPROTECT&BORDIS&IESODIS&FCMDIS);

////////////////////////////////////////////////////////////////////////
// Configuration

// PS2_CLK must be an RA-port! (wakeup/interrupt on change)
#define PIN_PS2_CLK			A0
#define PIN_PS2_DATA		A1
#define PIN_STROBE			A2

// pin-assignments

#define P_OUTPUT_BIT0		RC0
#define P_OUTPUT_BIT1		RC1
#define P_OUTPUT_BIT2		RC2
#define P_OUTPUT_BIT3		RC3
#define P_OUTPUT_BIT4		RC4
#define P_OUTPUT_BIT5		RC5
#define P_OUTPUT_BIT6		RA4
#define P_OUTPUT_BIT7		RA5

// two of the above bits is on PORTA, which is the TRIS-bit we need to set?
#define TRIS_PORTA_OUTPUT_BIT1	TRISA4
#define TRIS_PORTA_OUTPUT_BIT2	TRISA5

// enable if you're simulating/debugging
// #define SIMULATOR

////////////////////////////////////////////////////////////////////////
// you probably don't need to change anything below this line

#define P_PS2_CLK 			R##PIN_PS2_CLK
#define TRIS_PS2_CLK 		TRIS##PIN_PS2_CLK
#define IOC_PS2_CLK			IOC##PIN_PS2_CLK

#define P_PS2_DATA 			R##PIN_PS2_DATA
#define TRIS_PS2_DATA		TRIS##PIN_PS2_DATA

#define P_STROBE 			R##PIN_STROBE
#define TRIS_STROBE			TRIS##PIN_STROBE

// ��� ����, ����� �������: CapsLock, NumLock, ScrollLock
static const char led_anim[] = {
	0b000, 0b010, 0b110, 0b111, 0b101, 0b001, 0b000
};

unsigned char led_mod = 0; 	// ����������� ��������� �����������
unsigned char modifier = 0;	// ����������� ������ ����� ��������

#define MODIFIER_SHIFT		1
#define MODIFIER_CAPS		2
#define MODIFIER_SHF_CPS	3
#define MODIFIER_RUS		4
#define MODIFIER_RUS_SHF	5
#define MODIFIER_RUS_CPS	6
#define MODIFIER_RUS_SHF_CPS	7
#define MODIFIER_CTRL		8

#define LED_CAPS	0b100
#define LED_SCRL	0b001

int MCAPS = 0; //��������� ������� CAPS
int MSCRL = 0; //��������� ������� ScrollLock

//������ ����� � ����������
unsigned char ps2_read()
{
	char d = 0;
	char i;

	// ready for data, free bus
	P_PS2_CLK = 1;
	TRIS_PS2_CLK = 1;

	while( P_PS2_CLK != 0 ) {
		RAIF = 0;
		// before and after sleep the watchdog-timer is always cleared automatically
		SLEEP();
		// wakeup should take around 5us
	}

	// "read" start-bit
	while( P_PS2_DATA != 0 );
	while( P_PS2_CLK == 0 );
	while( P_PS2_CLK == 1 );
	
	for( i = 0; i < 8; i++ ) {
		while( P_PS2_CLK == 0 );
		// CLK went high, read bit
		d >>= 1;
		d |= ( P_PS2_DATA ) << 7;
		while( P_PS2_CLK == 1 );
	}
	
	// "read" parity
	while( P_PS2_CLK == 0 );
	while( P_PS2_CLK == 1 );
	// stopbit
	while( P_PS2_CLK == 0 );
	
	// lock bus
	P_PS2_CLK = 0;	
	TRIS_PS2_CLK = 0;

	CLRWDT();

	return d;
}

//������ ����� � ����������
void ps2_write( unsigned char d )
{
	char i;
	char parity = 1;

	CLRWDT();

	// 1) Bring the Clock line low for at least 100 microseconds. 
	P_PS2_CLK = 0;
	TRIS_PS2_CLK = 0;
	DelayUs( 110 );

	// 2) Bring the Data line low.
	P_PS2_DATA = 0;
	TRIS_PS2_DATA = 0;
	DelayUs( 30 );

	// 3) Release the Clock line.
	TRIS_PS2_CLK = 1;

	// 4) Wait for the device to bring the Clock line low.
	// 5) Set/reset the Data line to send the first data bit
	// 6) Wait for the device to bring Clock high.
	// 7) Wait for the device to bring Clock low.
	// 8) Repeat steps 5-7 for the other seven data bits and the parity bit
	for( i = 0; i < 8; i++ ) {
		while( P_PS2_CLK != 0 );
		P_PS2_DATA = d & 1;
		parity ^= ( d & 1 );	// "toggle if 1"
		d >>= 1;
		while( P_PS2_CLK != 1 );
	}

	while( P_PS2_CLK != 0 );
	P_PS2_DATA = parity & 1;
	while( P_PS2_CLK != 1 );
	while( P_PS2_CLK != 0 );
	
	// 9) Release the Data line.
	TRIS_PS2_DATA = 1;

	// 10) Wait for the device to bring Data low.
	while( P_PS2_DATA != 0 );
	// 11) Wait for the device to bring Clock low.
	while( P_PS2_CLK != 0 );
	// 12) Wait for the device to release Data and Clock
	while( P_PS2_CLK != 1 );
	while( P_PS2_DATA != 1 );

	CLRWDT();
}

//������ � ��������� ����������
void ps2_led( char l )
{
	ps2_write( 0xed );
	ps2_read();
	ps2_write( l );
	ps2_read();
}

//������������� ����������
void ps2_init()
{
	char i, j;
	// sleep 200 ms, give keyboard some time
	DelayMs( 200 );

	// after 500ms we should get the self-test result
	while( ps2_read() != 0xaa );

	// set typematic rate/delay
	ps2_write( 0xf3 );
	ps2_read();
	// 0.25 delay, 12 cps
	ps2_write( 0b001010 );
	ps2_read();
// �������� �������������� ����� �����, �������, �� ������ ;)
	for( i = 0; i < sizeof(led_anim); i++ ) {
		ps2_led( led_anim[ i ] );
		DelayMs( 200 );
	}

	CLRWDT();
}

// returns 0 if not found - ����� ������� �� ��������
signed char map_lookup( const unsigned char* map, unsigned char key, unsigned char size )
{
	// scanning takes less than 2ms in the biggest table at 4MHz
	unsigned char i;
	for( i = 0; i < size; i++ ) {
		if( map[ i*2 ] == key )
			return map[ i*2 + 1 ];
	}
	return 0;
}

//�������� ������� � ���������� � ����� ������������ ����� ��������
unsigned char getch()
{
	do {
		unsigned char k, br;
		signed char k_t;

		do {
			k = ps2_read();
		} while( k == 0xe0 );	// discard special prefix

		br = ( k == 0xf0 );		// is it a breakcode?
		if( br )
			k = ps2_read();		// yes -> read key
		
		// check modifiers
		switch( k ) {
		case SC_LSHIFT:
		case SC_RSHIFT:
			if( br )
				modifier &= ~MODIFIER_SHIFT;
			else
				modifier |= MODIFIER_SHIFT;
			continue;

		case SC_CTRL:
			if( br )
				modifier &= ~MODIFIER_CTRL;
			else 
				modifier |= MODIFIER_CTRL;		
			continue;

		case SC_CAPSLK:
			if( br ){
				if ( MCAPS == 0	) {
					led_mod |= LED_CAPS;
					modifier |= MODIFIER_CAPS;
					MCAPS = 1;
					ps2_led( led_mod );
				}
				else {
					led_mod &= ~LED_CAPS;
					modifier &= ~MODIFIER_CAPS;				
					MCAPS = 0;
					ps2_led( led_mod );
				}
			}
			continue;

		case SC_SCRLLK:
			if( br ){
				if ( MSCRL == 0 ) {
					led_mod |= LED_SCRL;
					modifier |= MODIFIER_RUS;
					MSCRL = 1;
					ps2_led( led_mod );
					}
				else {
					led_mod &= ~LED_SCRL;
					modifier &= ~MODIFIER_RUS;
					MSCRL = 0;
					ps2_led( led_mod );
					}
			}
			continue;
		}

		// normal key, only evaluate key-on
		if( br != 0 )
			continue;

		k_t = -1;

		switch( modifier ) {
		case 0:
			k_t = map_lookup( keymap, k, keymap_size );
			break;
		case 1:
			k_t = map_lookup( keymap_shift, k, keymap_shift_size );
			break;
		case 2:
			k_t = map_lookup( keymap_caps, k, keymap_caps_size );
			break;
		case 3:
			k_t = map_lookup( keymap_caps_shift, k, keymap_caps_shift_size );
			break;
		case 4:
			k_t = map_lookup( keymap_rus, k, keymap_rus_size );
			break;
		case 5:
			k_t = map_lookup( keymap_rus_shift, k, keymap_rus_shift_size );
			break;
		case 6:
			k_t = map_lookup( keymap_rus_caps, k, keymap_rus_caps_size );
			break;
		case 7:
			k_t = map_lookup( keymap_rus_caps_shift, k, keymap_rus_caps_shift_size );
			break;
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
			k_t = map_lookup( keymap_ctrl, k, keymap_ctrl_size );
			break;
		}
//		if( k_t >= 0 )
			return (unsigned char)k_t;
	}
	while( 1 );

	return 0;
}

//�������� ���� - ��������� ����, ������ ������� ASCII � ����
void main()
{
	OSCCON = 0b1100111;	//�� ���� ��������� ����������� ���������� 4 ��� � ���������� ������ �� ����
	OSCTUNE = 0;	//�� ���� ����������� ������� 4 ���
	PCON = 0;			// clear brownout/poweron-reset
	CMCON0 = 0b111;		// disable comparator
	ANSEL = 0;			// A/D off
	RAPU = 1;			// disable PORTA pull-ups

	// input
	TRIS_PS2_DATA = 1;
	TRIS_PS2_CLK = 1;

	// PORTC (6 pins) and two pins of PORTA are the outputs to the computer
	TRISC = 0;
	TRIS_PORTA_OUTPUT_BIT1 = 0;
	TRIS_PORTA_OUTPUT_BIT2 = 0;

	// data strobe
	P_STROBE = 1;	
	TRIS_STROBE = 0;

	// interrupt setup
	INTCON = 0;
	RAIE = 1;
	IOC_PS2_CLK = 1;

	ps2_init();

	while( 1 ) {
		unsigned char key = getch();

		if( key > 0 ){
	#define ASSIGN_AND_SHIFT(x) \
		P_OUTPUT_BIT##x = key & 1; \
		key >>= 1;

		ASSIGN_AND_SHIFT(0);
		ASSIGN_AND_SHIFT(1);
		ASSIGN_AND_SHIFT(2);
		ASSIGN_AND_SHIFT(3);
		ASSIGN_AND_SHIFT(4);
		ASSIGN_AND_SHIFT(5);
		ASSIGN_AND_SHIFT(6);
		ASSIGN_AND_SHIFT(7);

		// assert strobe
		P_STROBE = 0;
		DelayUs( 15 );
		P_STROBE = 1;
		}
	}
}
