;
; IDE Identify program
; Written Dec 1995, Peter W. Cervasio (cervasio@airmail.net)
; Sort of a translation of Tilmann Reh's Pascal program.
;
;

; CF registers
CF_DATA		.EQU	$10
CF_FEATURES	.EQU	$11
CF_ERROR	.EQU	$11
CF_SECCOUNT	.EQU	$12
CF_SECTOR	.EQU	$13
CF_CYL_LOW	.EQU	$14
CF_CYL_HI	.EQU	$15
CF_HEAD		.EQU	$16
CF_STATUS	.EQU	$17
CF_COMMAND	.EQU	$17
CF_LBA0		.EQU	$13
CF_LBA1		.EQU	$14
CF_LBA2		.EQU	$15
CF_LBA3		.EQU	$16

;CF Features
CF_8BIT		.EQU	1
CF_NOCACHE	.EQU	082H
;CF Commands
CF_READ_SEC	.EQU	020H
CF_WRITE_SEC	.EQU	030H
CF_SET_FEAT	.EQU 	0EFH

	ORG	5000H

BEGIN   LD      HL,WELMSG$
	call PRINT


;
; Initialize the drive
;
;        LD      A,6             ; Reset & Intr Enable
;	OUT	(CF_COMMAND),A	; Software reset
;	LD	B,0
;BEGXXX	DJNZ	BEGXXX		; Do a short time delay


	CALL	cfWait
	LD 	A,CF_8BIT	; Set IDE to be 8bit
	OUT	(CF_FEATURES),A
	LD	A,CF_SET_FEAT
	OUT	(CF_COMMAND),A


	CALL	cfWait
	LD 	A,CF_NOCACHE	; No write cache
	OUT	(CF_FEATURES),A
	LD	A,CF_SET_FEAT
	OUT	(CF_COMMAND),A

;
	CALL	IDEIDNT		; IDE Identify command
;
; Okay, show whatever we read.
;
        LD      HL,IDCNST$      ; String to show
        LD      DE,CONFIG      ; Integer to show
        CALL    SHOWINT         ; This should really be a macro!
;
        LD      HL,FIXCYL$      ; Show # of fixed cylinders
	LD	DE,NMCYL1
	CALL	SHOWINT
;
        LD      HL,REMCYL$      ; # of removable cylinders??
	LD	DE,NMCYL2
	CALL	SHOWINT
;
        LD      HL,NUMHED$      ; Physical # of heads
	LD	DE,NUMHED
	CALL	SHOWINT
;
        LD      HL,PHBYTK$      ; Physical # of tracks
	LD	DE,BYTRAK
	CALL	SHOWINT
;
        LD      HL,PHBYSC$      ; Physical # of bytes/sector
	LD	DE,BYSECT
	CALL	SHOWINT
;
        LD      HL,SECTRK$      ; Physical # of sectors/track
	LD	DE,SECTRK
	CALL	SHOWINT
;
        LD      HL,SERNUM$      ; Again, the description string
        LD      DE,SERNUM      ; The data (an IDE string)
        LD      B,20            ; Length of the string
        CALL    SHOWSTR         ; And show it
;
        LD      HL,CTLREV$      ; Controller revision
	LD	DE,CTLREV
	LD	B,8
	CALL	SHOWSTR
;
        LD      HL,CTLMDL$      ; Controller model
	LD	DE,CTLMDL
	LD	B,40
	CALL	SHOWSTR
;
        LD      HL,BUFSIZ$      ; Buffer size
	LD	DE,BUFSIZ
	CALL	SHOWINT
;
        LD      HL,BUFTYP$      ; Show buffer type message
	call 	PRINT
        LD      A,(BUFTYP)     ; Get the type from sector
        DEC     A               ; Was it a 1?
        JR      Z,ID0001        ; Jump
        DEC     A               ; How about a 2?
	JR	Z,ID0002
        DEC     A               ; Could it be a 3?
        JR      Z,ID0003        ; It must have been a 0.
;
; Load the correct string into HL.  The DB 0DDH turns any later
; instructions into LD IX,xxxx.

ID0000  LD      HL,BUFTY0$
        DB      0DDH
ID0001  LD      HL,BUFTY1$
	DB	0DDH
ID0002  LD      HL,BUFTY2$
	DB	0DDH
ID0003  LD      HL,BUFTY3$
	call 	PRINT
;
        LD      HL,CAPABL$
        LD      DE,CAPABL
        CALL    SHOWINT
;
        LD      HL,CBVAL$       ; Next stuff could be valid
        LD      A,(CURVAL)      ; Check bit 0
        OR      A
        JR      Z,MAYBE         ; 0 means might be valid
        LD      HL,YES$

        DB      0DDH            ; Turn LD HL into LD IX
MAYBE   LD      HL,MAYBE$
	call 	PRINT

        LD      HL,CURCYL$
        LD      DE,CURCYL
        CALL    SHOWINT

        LD      HL,CURHED$
        LD      DE,CURHED
        CALL    SHOWINT

        LD      HL,CURSEC$
        LD      DE,CURSEC
        CALL    SHOWINT

	RET
;

;================================================================================================
; Wait for disk to be ready (busy=0,ready=1)
;================================================================================================
cfWait:
		PUSH 	AF
cfWait1:
		in 	A,(CF_STATUS)
		AND 	080H
		cp 	080H
		JR	Z,cfWait1
		POP 	AF
		RET

;
; Send an IDE Identify command and read the data
;
IDEIDNT:
	 LD      A,0A0H          ; Drive 0
        OUT     (CF_HEAD),A      ; 
        CALL    cfWait         ; Wait for DRQ to be asserted

	 LD      A,0ECH          ; Command to cause IDE to identify itself
        OUT     (CF_COMMAND),A      ; Send it to the drive
        CALL    cfWait         ; Wait for DRQ to be asserted
        RET     NZ              ; Return if we waited too long

        LD      HL,CONFIG      ; Point to config buffer
        LD      BC,CF_DATA      ; B=0, C=Data port
        INIR                    ; First 256 bytes
        INIR                    ;   then the second

        CALL    cfWait         ; Make sure drive is ready now
        IN      A,(CF_STATUS)     ; Get status
        AND     89H             ; Mask off the proper bits???
        RET     Z               ; Return if okay
        PUSH    AF              ; Save error value
        LD      HL,ERRORM$      ; Point to error message
	call 	PRINT
        POP     AF              ; Recover the return value
        RET                     ; and do so
;
ERRORM$	DB	10,'Error reading IDE identify data.',10,13,0
;

;Input: HL = number to convert, DE = location of ASCII string
;Output: ASCII string at (DE)

Num2Dec	ld	bc,-10000
	call	Num1
	ld	bc,-1000
	call	Num1
	ld	bc,-100
	call	Num1
	ld	c,-10
	call	Num1
	ld	c,b

Num1	ld	a,'0'-1
Num2	inc	a
	add	hl,bc
	jr	c,Num2
	sbc	hl,bc

	ld	(de),a
	inc	de
	ret
;
; Show Text and integer value
;
SHOWINT PUSH    DE              ; Save the integer's address
	call 	PRINT
        POP     HL              ; Get integer's address back in HL
        LD      A,(HL)          ; Move contents of (HL) into HL
	INC	HL
	LD	H,(HL)
	LD	L,A
        LD      DE,HBUFF$       ; Point to a buffer to do the conversion
	call 	Num2Dec
        LD      HL,HBUFF$       ; Point to those numbers
	call 	PRINT
	RET
;
; Show a message and an IDE string value.  IDE strings are weird.
; They're stored with each "word" backwards.  "ABCD" is comes out
; as "BADC" from the drive.  WHY!?!?
;
SHOWSTR	PUSH	DE		; String to show in here
        PUSH    BC              ; Its length is in B
	call 	PRINT
	POP	BC		; Get length back
	POP	HL		; Get string back
LDLOOP1	INC	HL		; Show second char first
	LD	A,(HL)		; Get char
	RST	08H
	DEC	HL		; Point to prior char
	LD	A,(HL)		; Move it into C
	RST	08H
	INC	HL		; Point past those 2 chars
	INC	HL
        DEC     B               ; adjust for DJNZ (we did 2 chars)
        RET     Z               ; just in case it was an ODD value
	DJNZ	LDLOOP1		; Okay, loop for the rest
        
	call 	TXCRLF
	RET

;------------------------------------------------------------------------------
; Print string of characters to Serial A until byte=$00, WITH CR, LF
;------------------------------------------------------------------------------
PRINT		LD   A,(HL)	; Get character
		OR   A		; Is it $00 ?
		RET  Z		; Then RETurn on terminator
		RST  08H	; Print it
		INC  HL		; Next Character
		JR   PRINT	; Continue until $00


TXCRLF		LD   A,$0D	; 
		RST  08H	; Print character 
		LD   A,$0A	; 
		RST  08H	; Print character
		RET

;
; We really don't need this ORG statement.  I just wanted a nice
; out of the way location to store the sector data so I could look
; at it with my debugger.  Being up here keeps it from getting
; clobbered too much.
;
;        ORG     8000H
;
;
; Buffer for doing integer to ASCII conversion
;
HBUFF$	DB	'     ',13,10,0

WELMSG$	DB	'IDE Test program - Version 01.00.00',13,10
        DB      'Sort of translated from T. REH''s Pascal'
        DB      ' program by Pete Cervasio.',13,10,10,0
;
IDCNST$ DB      'ID Constant            : ',0
FIXCYL$ DB      'Fixed Cylinders        : ',0
REMCYL$ DB      'Removable Cylinders    : ',0
NUMHED$ DB      'Number of heads        : ',0
PHBYTK$ DB      'Phys Bytes / Track     : ',0
PHBYSC$ DB      'Phys Bytes / Sector    : ',0
SECTRK$ DB      'Sectors per track      : ',0
SERNUM$ DB      'Serial number          : ',0
CTLREV$ DB      'Controller revision    : ',0
CTLMDL$ DB      'Controller model       : ',0
BUFSIZ$ DB      'Buffer size            : ',0
BUFTYP$ DB      'Controller type        : ',0
CAPABL$ DB      'Capabilities (Bit9=LBA): ',0
CBVAL$  DB      'Next items valid       : ',0
CURCYL$ DB      'Current # Cylinders    : ',0
CURSEC$ DB      'Current # Sectors      : ',0
CURHED$ DB      'Current # Heads        : ',0
BUFTY0$ DB      'Not specified',13,10,0
BUFTY1$ DB      'Single port, single sector',13,10,0
BUFTY2$ DB      'Dual port, multi sector',13,10,0
BUFTY3$ DB      'Dual port, multi sector, look-ahead',13,10,0
YES$    DB      'Yes',13,10,0
MAYBE$  DB      'Maybe',13,10,0

;
; First, we need a place to read the data into
;
SECBUF	EQU	$		; 512 byte sector start
;
; The following descriptions of each word in the identify table is
; taken from the IDE standard found on the World Wide Web.
;
;-------+-------------------------------------------------------
; Word  | Bit and description
;-------|-------------------------------------------------------
;    0  | General configuration bit-significant information:     I got
;       |   15  0   reserved for non-magnetic drives               0
;       |   14  1=format speed tolerance gap required              1
;       |   13  1=track offset option available                    0
;       |   12  1=data strobe offset option available              0
;       |   11  1=rotational speed tolerance is > 0,5%             0
;       |   10  1=disk transfer rate > 10 Mbs                      0
;       |    9  1=disk transfer rate > 5Mbs but <= 10Mbs           1
;       |    8  1=disk transfer rate <= 5Mbs                       0
;       |    7  1=removable cartridge drive                        0
;       |    6  1=fixed drive                                      1
;       |    5  1=spindle motor control option implemented         0
;       |    4  1=head switch time > 15 usec                       1
;       |    3  1=not MFM encoded                                  1
;       |    2  1=soft sectored                                    0
;       |    1  1=hard sectored                                    1
;       |    0  0=reserved                                         0
;
CONFIG DW      0               ; I get 16986 (425AH, 0100001001011010b)
;
;    1  | Number of cylinders
;
NMCYL1 DW      0               ; I get 790
;
;    2  | Reserved
;
NMCYL2 DW      0               ; I get 0
;
;    3  | Number of heads
;
NUMHED DW      0               ; I get 15
;
;    4  | Number of unformatted bytes per track
;
BYTRAK DW      0               ; I get 36366
;
;    5  | Number of unformatted bytes per sector
;
BYSECT DW      0               ; I get 638
;
;    6  | Number of sectors per track
;
SECTRK DW      0               ; I get 57
;
;  7-9  | Vendor unique

D1     DW      0
D2	DW	0
D3	DW	0
;
; 10-19 | Serial number (20 ASCII characters, 0000h=not specified)
;
SERNUM DC      20,0            ; I get nothing...
;
;   20  | Buffer type
;
BUFTYP DW      0               ; I get a 3
;
;   21  | Buffer size in 512 byte increments (0000h=not specified)
;
BUFSIZ DW      0               ; I get 128 (that would be 64k)
;
;   22  | # of ECC bytes avail on read/write long cmds (0000h=not spec'd)
;
ECCBYT DW      0
;
; 23-26 | Firmware revision (8 ASCII characters, 0000h=not specified)
;
CTLREV DC      8,0             ; I get "BADH0956"
;
; 27-46 | Model number (40 ASCII characters, 0000h=not specified)
;
CTLMDL DC      40,0            ; I get "Maxtor 7345 AT"
;
;   47  | 15-8  Vendor unique
;       |  7-0  00h = Read/write multiple commands not implemented
;       |       xxh = Maximum number of sectors that can be transferred
;       |             per interrupt on read and write multiple commands
;
SECINT DW      0
;
;   48  | 0000h = cannot perform doubleword I/O    Included for backwards
;       | 0001h = can perform doubleword I/O       Compatible VU use
;
DBWFLG DW      0               ; Can do double word I/O if 1
;
;   49  | Capabilities
;       | 15-10  0=reserved
;       |     9  1=LBA supported
;       |     8  1=DMA supported
;       |  7- 0  Vendor unique
;
CAPABL DW      0               ; I get 256 (Bit 8=1)
;
;   50  | Reserved
;
RES000 DW      0
;
;   51  | 15-8  PIO data transfer cycle timing mode
;       |  7-0  Vendor unique
;
RES001 DW      0
;
;   52  | 15-8  DMA data transfer cycle timing mode
;       |  7-0  Vendor unique
;
RES002 DW      0
;
;   53  | 15-1  Reserved
;       |    0  1=the fields reported in words 54-58 are valid
;       |       0=the fields reported in words 54-58 may be valid
;
CURVAL DW      0               ; Didn't check.
;
;   54  | Number of current cylinders
;
CURCYL DW      0               ; I got 670!  Docs said it was 654!?!
;
;   55  | Number of current heads
;
CURHED DW      0               ; I got 16
;
;   56  | Number of current sectors per track
;
CURSEC DW      0               ; I got 63
;
; 57-58 | Current capacity in sectors
;   59  | 15-9  Reserved
;       |    8  1 = Multiple sector setting is valid
;       |  7-0  xxh = Current setting for number of sectors that can be
;       |             transferred per interrupt on R/W multiple commands
; 60-61 | Total number of user addressable sectors (LBA mode only)
;   62  | 15-8  Single word DMA transfer mode active
;       |  7-0  Single word DMA transfer modes supported (see 11-3a)
;   63  | 15-8  Multiword DMA transfer mode active
;       |  7-0  Multiword DMA transfer modes supported (see 11-3b)
; 64-127| Reserved
;128-159| Vendor unique
;160-255| Reserved
;
;
;
RESTOF DC      200H,0          ; Fill it out with some extra space
;
;
;

;
	END	BEGIN
