;------------------------------------------------------------------------------
; General Equates
;------------------------------------------------------------------------------


SER_BUFSIZE	.EQU	20h
SER_FULLSIZE	.EQU	10H
SER_EMPTYSIZE	.EQU	5

RTS_HIGH	.EQU	05H
RTS_LOW		.EQU	025H ; ��� ��51

SIOA_D		.EQU	$00 ; �����+����� ������ - ����� SIOA_D
SIOA_C		.EQU	$02 ; �����+����� ����������+��������� - ����� ��� SIOA_C-1
SIOB_D		.EQU	$01 ;  ��51 ������ - ����� ��� SIOB_D+7
SIOB_C		.EQU	$03 ; ��51 ��������� - ����� SIOB_C+6

		ORG	$4000
serBBuf		.ds	SER_BUFSIZE
serBInPtr	.ds	2
serBRdPtr	.ds	2
serBBufUsed	.ds	1


	ORG	5000H

INIT
		DI				; Disable interrupts.
		LD	SP,biosstack		; Set default stack.

;		Turn off ROM

		LD	A,$01
		OUT ($38),A


		; Interrupt vector in page FF
		LD	A,$FF
		LD	I,A

		IM 2
		EI


		xor	a			;0 to accumulator
		LD	(serBBufUsed),A

		LD	HL,serBBuf
		LD	(serBInPtr),HL
		LD	(serBRdPtr),HL

		LD      HL,WELMSG$
		call PRINT

;	Initialise SIO

		XOR A
            OUT (SIOB_C+6),A
		nop
		nop
		nop
            OUT (SIOB_C+6),A
		nop
		nop
		nop
            OUT (SIOB_C+6),A	; ������� ��������� ��51 ���������� ������-��, ����� �� ��������
		nop
		nop
		nop
            LD A,$40   ;����������� �����
            OUT (SIOB_C+6),A
		nop
		nop
		nop
	ld	a,01001110b	;8251 MODE Instruction
;		  ||||||++-----   Baud Rate :*16  ( 500KHz/16 = 31.25KHz )
;		  ||||++-------   Character Length : 8bits
;		  ||++---------   Parity Disable
;		  ++-----------   Stop Bit : 1bit
;
            OUT (SIOB_C+6),A
		nop
		nop
		nop
	ld	a,00100110b	;8251 COMMAND Instruction
;		  |||||||+-----   Transmit : Enable
;		  ||||||+------   ~DTR = LOW : 8253timer enable
;		  |||||+-------   Receive  : Enable
;		  ||||+--------   Send Break Character : Normal
;		  |||+---------   Error Reset : No Operation
;		  ||+----------   ~RTS = LOW : MIDI IN enable
;		  |+-----------   No Operation
;		  +------------   No Operation
;
            OUT (SIOB_C+6),A
		nop
		nop
		nop
		IN A,(SIOB_D+7)
		IN A,(SIOB_D+7)
		;IN A,(SIOB_D+7) ; �������


lp
	call conin
	call conout
	jmp  lp
	ret

;------------------------------------------------------------------------------
; Print string of characters to Serial A until byte=$00, WITH CR, LF
;------------------------------------------------------------------------------
PRINT		LD   A,(HL)	; Get character
		OR   A		; Is it $00 ?
		RET  Z		; Then RETurn on terminator
		call conout
;		RST  08H	; Print it
		INC  HL		; Next Character
		JR   PRINT	; Continue until $00


TXCRLF		LD   A,$0D	; 
		call conout
;		RST  08H	; Print character 
		LD   A,$0A	; 
		call conout
		;RST  08H	; Print character
		RET

WELMSG$	DB	'UART Test programm. Switch your terminal to 38400 8N1...',10,0

;------------------------------------------------------------------------------
; Serial interrupt handlers
; Same interrupt called if either of the inputs receives a character
; so need to check the status of each SIO input.
;------------------------------------------------------------------------------
serialInt:	DI
		PUSH	AF
		PUSH	HL

serialIntB:
		LD	HL,(serBInPtr)
		INC	HL
		LD	A,L
		CP	(serBBuf+SER_BUFSIZE) & $FF
		JR	NZ, notBWrap
		LD	HL,serBBuf
notBWrap:
		LD	(serBInPtr),HL
		IN	A,(SIOB_D+7)
		LD	(HL),A

		LD	A,(serBBufUsed)
		INC	A
		LD	(serBBufUsed),A
		CP	SER_FULLSIZE
		JR	C,rtsB0
            	LD   	A,RTS_HIGH
		OUT  	(SIOB_C+6),A
rtsB0:
		POP	HL
		POP	AF
		EI
		RETI

;------------------------------------------------------------------------------
; Console input routine
; Use the "primaryIO" flag to determine which input port to monitor.
;------------------------------------------------------------------------------
conin:
		PUSH	HL

waitForCharB:
		LD	A,(serBBufUsed)
		CP	$00
		JR	Z, waitForCharB
		LD	HL,(serBRdPtr)
		INC	HL
		LD	A,L
		CP	(serBBuf+SER_BUFSIZE) & $FF
		JR	NZ, notRdWrapB
		LD	HL,serBBuf
notRdWrapB:
		DI
		LD	(serBRdPtr),HL

		LD	A,(serBBufUsed)
		DEC	A
		LD	(serBBufUsed),A

		CP	SER_EMPTYSIZE
		JR	NC,rtsB1
            	LD   	A,RTS_LOW
		OUT  	(SIOB_C+6),A
rtsB1:
		LD	A,(HL)
		POP	HL
		EI
		RET	; Char ready in B

;------------------------------------------------------------------------------
; Console output routine
; Use the "primaryIO" flag to determine which output port to send a character.
;------------------------------------------------------------------------------
conout:	

conoutB:
		PUSH	AF


conoutB1:	IN   	A,(SIOB_C+6)	; ������ ���� D2=1 �������� ���������	
		AND 	00000100B			; ����� 	
		JR	Z,conoutB1	; Loop until SIO flag signals ready
		POP	AF		; RETrieve character
		OUT	(SIOB_D+7),A	; OUTput the character
		RET

;------------------------------------------------------------------------------
; Check if there is a character in the input buffer
; Use the "primaryIO" flag to determine which port to check.
;------------------------------------------------------------------------------
CKINCHAR
ckincharB:

		LD	A,(serBBufUsed)
		CP	$0
		RET

		.DS	020h		; Start of BIOS stack area.
biosstack:	.EQU	$

;=================================================================================
;������ ���������� �� IM2
;=================================================================================
		.ORG 0FFFAH
		.dw serialInt
.END