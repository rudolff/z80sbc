;������ ��������� ��/� � ������� �� �����������
; ������ - ���� �������� ��� ������ �����, �� �������� ��������
; ��������� �����/������ ���������� ��� ��51, ASCII-����� � ���������� �� ������
; ��51 ������ ����������������, �� �� ������������, ��� ��� ����� �������� ���-��� ���
; ����� �������� ������� �����
;===============================================
;------------------------------------------------------------------------------
; General Equates
;------------------------------------------------------------------------------

CR		.EQU	0DH
LF		.EQU	0AH
ESC		.EQU	1BH
CTRLC		.EQU	03H
CLS		.EQU	0CH

; CF registers
CF_DATA		.EQU	$10
CF_FEATURES	.EQU	$11
CF_ERROR	.EQU	$11
CF_SECCOUNT	.EQU	$12
CF_SECTOR	.EQU	$13
CF_CYL_LOW	.EQU	$14
CF_CYL_HI	.EQU	$15
CF_HEAD		.EQU	$16
CF_STATUS	.EQU	$17
CF_COMMAND	.EQU	$17
CF_LBA0		.EQU	$13
CF_LBA1		.EQU	$14
CF_LBA2		.EQU	$15
CF_LBA3		.EQU	$16

;CF Features
CF_8BIT		.EQU	1
CF_NOCACHE	.EQU	082H
;CF Commands
CF_READ_SEC	.EQU	020H
CF_WRITE_SEC	.EQU	030H
CF_SET_FEAT	.EQU 	0EFH


loadAddr	.EQU	0cf00h	; CP/M load address
numSecs		.EQU	25	; Number of 512 sectors to be loaded


SER_BUFSIZE		.EQU	40H
SER_FULLSIZE	.EQU	30H
SER_EMPTYSIZE	.EQU	5

RTS_HIGH	.EQU	05H
RTS_LOW	.EQU	025H ; ��� ��51

SIOA_D		.EQU	$00 ; �����+����� ������ - ����� SIOA_D
SIOA_C		.EQU	$02 ; �����+����� ����������+��������� - ����� ��� SIOA_C-1
SIOB_D		.EQU	$01 ;  ��51 ������ - ����� ��� SIOB_D+7
SIOB_C		.EQU	$03 ; ��51 ��������� - ����� SIOB_C+6
		.ORG	$4000
serABuf		.ds	SER_BUFSIZE
serAInPtr	.ds	2
serARdPtr	.ds	2
serABufUsed	.ds	1
serBBuf		.ds	SER_BUFSIZE
serBInPtr	.ds	2
serBRdPtr	.ds	2
serBBufUsed	.ds	1

primaryIO	.ds	1
secNo		.ds	1
dmaAddr		.ds	2

stackSpace	.ds	32
STACK   	.EQU    $	; Stack top


;------------------------------------------------------------------------------
;                         START OF MONITOR ROM
;------------------------------------------------------------------------------

MON		.ORG	$0000		; MONITOR ROM RESET VECTOR
;------------------------------------------------------------------------------
; Reset
;------------------------------------------------------------------------------
RST00		DI			;Disable INTerrupts
		JP	INIT		;Initialize Hardware and go
		NOP
		NOP
		NOP
		NOP
;------------------------------------------------------------------------------
; TX a character over RS232 wait for TXDONE first.
;------------------------------------------------------------------------------
RST08		JP	conout
		NOP
		NOP
		NOP
		NOP
		NOP



;------------------------------------------------------------------------------
; Console output routine
; Use the "primaryIO" flag to determine which output port to send a character.
;------------------------------------------------------------------------------
conout:		PUSH	AF		; Store character
		LD	A,(primaryIO)
		CP	0
		JR	NZ,conoutB1
		JR	conoutA1
conoutA:
		PUSH	AF

conoutA1:	IN   	A,(SIOA_C-1)	; ������ ���� D1=1 �������� ���������	
		AND 00000010B			; ����� 
		JR	Z,conoutA1	; Loop until SIO flag signals ready
		POP	AF		; RETrieve character
		OUT	(SIOA_D),A	; OUTput the character
		RET

conoutB:
		PUSH	AF


conoutB1:	IN   	A,(SIOB_C+6)	; ������ ���� D2=1 �������� ���������	
		AND 00000100B			; ����� 	
		JR	Z,conoutB1	; Loop until SIO flag signals ready
		POP	AF		; RETrieve character
		OUT	(SIOB_D+7),A	; OUTput the character
		RET


;------------------------------------------------------------------------------
; Initialise hardware and start main loop
;------------------------------------------------------------------------------
INIT		LD   SP,STACK		; Set the Stack Pointer

		LD	HL,serABuf
		LD	(serAInPtr),HL
		LD	(serARdPtr),HL

		LD	HL,serBBuf
		LD	(serBInPtr),HL
		LD	(serBRdPtr),HL

		xor	a			;0 to accumulator
		LD	(serABufUsed),A
		LD	(serBBufUsed),A

;	Initialise SIO

		XOR A
            OUT (SIOB_C+6),A
            OUT (SIOB_C+6),A
            OUT (SIOB_C+6),A
            LD A,$40   ;����������� �����
            OUT (SIOB_C+6),A
            LD A,$4F  ;8 ���, 1 ����-���, ��� �������� ��������, �������� 1:64
            OUT (SIOB_C+6),A
            LD A,$35  ; �������� ����� � ��������, ������ RTS_LOW � ����� ��������� ������
            OUT (SIOB_C+6),A
		IN A,(SIOB_D+7)
		IN A,(SIOB_D+7)
		IN A,(SIOB_D+7) ; �������
;���������� ������� ��������� ������
		XOR A
		OUT (SIOA_D),A
;�� ������ ������ ������� �����, ������ � ������ (���� ���������� � ������)
		IN A,(SIOA_D)

		IM	1
		EI

		; ��������� ��� ��������������
		LD	A,$00
		LD	(primaryIO),A

;------------------------------------------------------------------------------
; CP/M load command
;------------------------------------------------------------------------------
CPMLOAD
    		LD HL,CPMTXT2
		CALL PRINT


		CALL	cfWait
		LD 	A,CF_8BIT	; Set IDE to be 8bit
		OUT	(CF_FEATURES),A
		LD	A,CF_SET_FEAT
		OUT	(CF_COMMAND),A


		CALL	cfWait
		LD 	A,CF_NOCACHE	; No write cache
		OUT	(CF_FEATURES),A
		LD	A,CF_SET_FEAT
		OUT	(CF_COMMAND),A

		LD	B,numSecs

		LD	A,0
		LD	(secNo),A
		LD	HL,loadAddr
		LD	(dmaAddr),HL
processSectors:

		CALL	cfWait

		LD	A,(secNo)
		OUT 	(CF_LBA0),A
		LD	A,0
		OUT 	(CF_LBA1),A
		OUT 	(CF_LBA2),A
		LD	a,0E0H
		OUT 	(CF_LBA3),A
		LD 	A,1
		OUT 	(CF_SECCOUNT),A

		call	read

		LD	DE,0200H
		LD	HL,(dmaAddr)
		ADD	HL,DE
		LD	(dmaAddr),HL
		LD	A,(secNo)
		INC	A
		LD	(secNo),A

		djnz	processSectors

; Start CP/M using entry at top of BIOS
; The current active console stream ID is pushed onto the stack
; to allow the CBIOS to pick it up
; 0 = SIO A, 1 = SIO B
		
		ld	A,(primaryIO)
		PUSH	AF
		ld	HL,($FFFE)
		jp	(HL)


;------------------------------------------------------------------------------

; Read physical sector from host

read:
		PUSH 	AF
		PUSH 	BC
		PUSH 	HL

		CALL 	cfWait

		LD 	A,CF_READ_SEC
		OUT 	(CF_COMMAND),A

		CALL 	cfWait

		LD 	c,4
		LD 	HL,(dmaAddr)
rd4secs:
		LD 	b,128
rdByte:
		nop
		nop
		in 	A,(CF_DATA)
		LD 	(HL),A
		iNC 	HL
		dec 	b
		JR 	NZ, rdByte
		dec 	c
		JR 	NZ,rd4secs

		POP 	HL
		POP 	BC
		POP 	AF

		RET


; Wait for disk to be ready (busy=0,ready=1)
cfWait:
		PUSH 	AF
cfWait1:
		in 	A,(CF_STATUS)
		AND 	080H
		cp 	080H
		JR	Z,cfWait1
		POP 	AF
		RET


;------------------------------------------------------------------------------
; Print string of characters to Serial A until byte=$00, WITH CR, LF
;------------------------------------------------------------------------------
PRINT		LD   A,(HL)	; Get character
		OR   A		; Is it $00 ?
		RET  Z		; Then RETurn on terminator
		RST  08H	; Print it
		INC  HL		; Next Character
		JR   PRINT	; Continue until $00


TXCRLF		LD   A,$0D	; 
		RST  08H	; Print character 
		LD   A,$0A	; 
		RST  08H	; Print character
		RET

CPMTXT2
		.BYTE	$0D,$0A
		.TEXT	"Loading CP/M..."
		.BYTE	$0D,$0A,$00


;------------------------------------------------------------------------------
FINIS		.END	

