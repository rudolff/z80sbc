#include <stdio.h>

#define SPI_PORT_ADDR 0x30
#define  SpiPortA (SPI_PORT_ADDR+0)
#define  SpiPortB (SPI_PORT_ADDR+1)
#define  SpiPortC (SPI_PORT_ADDR+2)
#define  SpiPortControl (SPI_PORT_ADDR+3)


void delay(int ms)
{
  while(ms--)
  {
#asm
      nop
      nop
      nop
      nop
      nop
      nop
      nop
      nop
#endasm
  }


}



int main()
{
  int i;
  outp(SpiPortControl, 0x80);
  printf("Hello World!");
  for(i=0; i<1000; i++)
  {
    // we can turn on or off each line of port C
    outp(SpiPortControl, (0*2)+1);
    delay(1000);
    outp(SpiPortControl, (1*2)+1);
    delay(1000);
    outp(SpiPortControl, (2*2)+1);
    delay(1000);
    outp(SpiPortControl, (3*2)+1);
    delay(1000);
    outp(SpiPortControl, (4*2)+1);
    delay(1000);
    outp(SpiPortControl, (5*2)+1);
    delay(1000);
    outp(SpiPortControl, (6*2)+1);
    delay(1000);
    outp(SpiPortControl, (7*2)+1);
    delay(1000);
    outp(SpiPortControl, (0*2)+0);
    delay(1000);
    outp(SpiPortControl, (1*2)+0);
    delay(1000);
    outp(SpiPortControl, (2*2)+0);
    delay(1000);
    outp(SpiPortControl, (3*2)+0);
    delay(1000);
    outp(SpiPortControl, (4*2)+0);
    delay(1000);
    outp(SpiPortControl, (5*2)+0);
    delay(1000);
    outp(SpiPortControl, (6*2)+0);
    delay(1000);
    outp(SpiPortControl, (7*2)+0);
    delay(1000);
  }
  return 0;
}
