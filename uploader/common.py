#!/usr/bin/python

import serial
import configparser
import os, sys
from stat import *
import glob

config = configparser.ConfigParser()
config.read('config.ini')

def walktree(top, callback):
  files = []
  if top.find("*") > -1 or top.find("?") > -1:
    # print('Search by mask "%s"' % top)
    files = glob.glob(top) 
  else:
    if S_ISREG(os.stat(top).st_mode): # is file
      files = [top]
    else:
      files = list(map(lambda f: os.path.join(top, f), os.listdir(top)))

  for pathname in files:
    stat = os.stat(pathname)

    if S_ISREG(stat.st_mode):
      # It's a file, call the callback function
      if stat.st_size > (512*1024):
        print('Skipping too big file %s' % pathname)
      else:
        callback(pathname)
    else:
      # Unknown file type, print a message
      print('Skipping %s' % pathname)

def upload(data):
  ser = serial.Serial(config['DEFAULT']['PORT'], config['DEFAULT']['BAUDRATE'], timeout=0,rtscts=1)  # open serial port
  resp = ''
  for i in range(10): # flush input buffer
    resp = ser.read(size=16)
    print(resp.decode("utf-8"), end='') 

  i = 0
  buf_size=256
  while True:
    chunk = data[i:i+buf_size]
    if(len(chunk) == 0):
      break
    i+=buf_size
    ser.write(chunk)
    ser.flush()
    resp = ser.read(size=16)
    if(resp):
      print(resp.decode("utf-8"), end='', flush=True)

  resp = ''
  while True: # wait for reset cp/m
    data = ser.read()
    resp += data.decode("utf-8") 
    if(data == b'>'): # wait for '>' character
      break
      
  print(resp, flush=True)
  ser.close()        

def ch_drive(letter):
  ser = serial.Serial(config['DEFAULT']['PORT'], config['DEFAULT']['BAUDRATE'], timeout=0,rtscts=1)  # open serial port
  resp = '';
  for i in range(10): # flush input buffer
    resp = ser.read(size=16)
    print(resp.decode("utf-8"), end='') 

  ser.write(bytes(letter, "utf-8") + b':\r\n')

  resp = '';
  while True:  # wait for reset cp/m
    data = ser.read()
    resp += data.decode("utf-8") 
    if(data == b'>'): # wait for '>' character
      break
      
  print(resp, flush=True)
  ser.close()  

