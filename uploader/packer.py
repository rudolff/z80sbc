#!/usr/bin/python

import common
import os, sys

def convertfile(filename):
  print('A:DOWNLOAD', os.path.basename(filename))
  print('U0')  
  file = open(filename, 'rb')
  data = file.read()
  out = ':'
  count = len(data)
  if(count % 128):
    add_size = 128 - (count % 128)
    count += add_size
    for i in range(add_size):
      data += b'\0'
      
  sum = 0
  for byte in data:
    out += '%0.2X' % byte
    sum += byte
  out += '>'
  out += '%0.2X' % (count % 256)
  out += '%0.2X' % (sum % 256)
  
  print(out)
  file.close()

if __name__ == '__main__':
  common.walktree(sys.argv[1], convertfile)
  