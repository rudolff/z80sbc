#!/usr/bin/python

import common
import os, sys
from stat import *

def readfile(filename):
  file = open(filename, 'r')
  print(filename)
  out = b''
  while True:
    line = file.readline()
    if(len(line) == 0):
      break
    out += bytes(line, "utf-8") 
    if line.startswith(':'):
      common.upload(out)
      out = b''

  file.close()

if __name__ == '__main__':
  if len(sys.argv) > 2:
    if len(sys.argv[2]) == 1 and ((sys.argv[2] >= 'a' and sys.argv[2] <= 'p') or (sys.argv[2] >= 'A' and sys.argv[2] <= 'P')):
      common.ch_drive(sys.argv[2])
    else:
      print('Wrong drive letter "%s"' % sys.argv[2])
      exit()
  readfile(sys.argv[1])
  