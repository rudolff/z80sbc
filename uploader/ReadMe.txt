Some python scripts to upload files to the sbc

requre installed python3 and pyserial library

python -m pip install pyserial

examples:

upload all files from some dir to SBC
python .\uploaddir.py ..\..\Z80ASM

upload files by mask *.com from some dir to disk D: on SBC
python .\uploaddir.py ..\..\Z80ASM\*.com d

prepare a package from some directory
python .\packer.py ..\..\Z80ASM > .\z80ASM.txt

upload package file from to SBC
python .\uploadpkg.py ..\transientAppsPackage\CPM211FilesPkg.txt
