#!/usr/bin/python

import common
import os, sys

def convertfile(filename):
  out = b'A:DOWNLOAD ' + bytes(os.path.basename(filename), encoding = 'utf-8') + b'\r\n' 
  out += b'U0\r\n' 
  file = open(filename, 'rb')
  data = file.read()
  out += b':'
  count = len(data)
  if(count % 128):
    add_size = 128 - (count % 128)
    count += add_size
    for i in range(add_size):
      data += b'\0'
      
  sum = 0
  for byte in data:
    out += b'%0.2X' % byte
    sum += byte
  out += b'>'
  out += b'%0.2X' % (count % 256)
  out += b'%0.2X' % (sum % 256)
  out += b'\r\n'
  #print('Uploading "%s"' % filename)
  common.upload(out)
  file.close()

if __name__ == '__main__':
  if len(sys.argv) > 2:
    if len(sys.argv[2]) == 1 and ((sys.argv[2] >= 'a' and sys.argv[2] <= 'p') or (sys.argv[2] >= 'A' and sys.argv[2] <= 'P')):
      common.ch_drive(sys.argv[2])
    else:
      print('Wrong drive letter "%s"' % sys.argv[2])
      exit()
  common.walktree(sys.argv[1], convertfile)
  